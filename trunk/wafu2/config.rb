# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/

module Wafu
  class ConfigHash < Hash # :nodoc:
    # キーが見つからない時は例外
    alias_method :[], :fetch

    # 上書きも不可
    alias_method :orig_store, :store
    def store key, value
      raise TypeError if !key.is_a?(String)
      raise ArgumentError if has_key?(key)

      orig_store key, value
    end
    alias_method :[]=, :store
  end

  class Config
    # あとでclass_variable_get() される
    @@hash = ConfigHash.new

    class << self
      def set key, value
        @@hash.store key, value
      end
    end
  end
end
