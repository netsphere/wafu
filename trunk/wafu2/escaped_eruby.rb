# -*- coding: utf-8 -*-

# このファイルは、過去との互換性のため。
# $USE_ERUBIS を定義する。

require "rubygems"
require "active_support/core_ext/string/output_safety" # override ERB::Util.h()





begin
  require "erubis/engine/eruby"
  $USE_ERUBIS = true
rescue LoadError
end

if $USE_ERUBIS

module Wafu
  # Escaped ERuby
  # Erubis::Basic::Converterを変更する
  # <%= と <%== の両方をエスケープする
  class EscapedEruby < Erubis::Eruby
    def self.desc   # :nodoc:
      "escape 'unsafe' string"
    end 

    def add_expr src, code, indicator
      if indicator == "=" || indicator == "=="
        add_expr_escaped src, code # 実行時にしか判定できない
      elsif indicator == "==="
        add_expr_debug src, code
      else
        raise ArgumentError
      end
    end


    # html_safe? によってエスケープするかどうかを決める
    def escaped_expr code
      return "ERB::Util.h(#{code})"
    end
  end # EscapedEruby

end

end # $USE_ERUBIS
