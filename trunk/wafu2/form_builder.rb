# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/


require "wafu2/tag_helper"


module Wafu

  # フォームコントロール関連のHTML片を生成する. form or form_for ブロック内で利用可能
  class FormBuilder
    include TagHelper

    # @param [String] object_name  更新する変数名. 先頭の"@"は不要
    def initialize context, object_name
      raise TypeError if object_name && !object_name.is_a?(String)

      @context = context
      @object_name = object_name
    end

    # チェックボックス. input[type="checkbox"]
    # @param [Hash] options
    # @option options [String] :label  ラベル. タグでもよい
    def check_box field_name, options = {}
      raise TypeError if !field_name.is_a?(String)
      raise ArgumentError, 
                   "cannnot multiple field" if field_name[-2..-1] == "[]"

      attrs = options.dup
      label = attrs.delete :label

      # checked属性で
      s = empty_tag("input", :type => "hidden", 
                    :name => name_attr(field_name), :value => "0")
      s << empty_tag( "input", {:type => "checkbox", 
                  :id => id_attr(field_name),
                  :name =>  name_attr(field_name),
                  :value => "1", # 必須
                  :checked => object_value(field_name) ? "checked" : nil 
          }.update(attrs) )
      if label
        s << start_tag( "label", :for => id_attr(field_name) )
        s << h(label) << "</label>".html_safe
      end
      field_with_error field_name, s
    end

    # input[type="file"]
    def file_field field_name, options = {}
      raise TypeError if !field_name.is_a?(String)
      
      field_with_error field_name, 
                       input_tag_object_value("file", field_name, options)
    end

    def hidden_field field_name, options = {}
      raise TypeError if !field_name.is_a?(String)

      input_tag_object_value "hidden", field_name, options
    end

    # input[type="password"]
    def password_field field_name, options = {}
      raise TypeError if !field_name.is_a?(String)

      field_with_error field_name, 
                       input_tag_object_value("password", field_name, options)
    end

    # ラジオボタン ... 一つの候補 input[type="radio"]
    # @param [Hash] options
    # @option options [String] :label  ラベル
    def radio_button field_name, tag_value, options = {}
      raise TypeError if !field_name.is_a?(String)

      attrs = options.dup
      label = attrs.delete :label

      # check属性で
      r = empty_tag "input", {:type => "radio", 
            :id => id_attr(field_name), # TODO:ダブる??
            :name => name_attr(field_name), 
            :value => tag_value,
            :checked => object_value(field_name) == tag_value ? "checked" : nil,
          }.update(attrs)
      if label
        r << start_tag( "label", :for => id_attr(field_name) )
        # タグの場合はエスケープしない
        r << h(label) << "</label>".html_safe
      end
      field_with_error field_name, r
    end

    # textareaタグ
    def text_area field_name, options = {}
      raise TypeError if !field_name.is_a?(String)

      s = start_tag "textarea", {
            :id => id_attr(field_name), :name => name_attr(field_name)
          }.update(options)
      # いつでもエスケープ
      s << Erubis::XmlHelper.escape_xml(object_value field_name)
      s << "</textarea>".html_safe

      field_with_error field_name, s
    end

    def text_field field_name, options = {}
      raise TypeError if !field_name.is_a?(String)

      field_with_error field_name, 
                       input_tag_object_value("text", field_name, options)
    end

    # select, optionタグ
    #
    # @example 
    #   <%= select "foo", :class => "green" do
    #         [ ["name1", 1], ["name2", 2], ... ]
    #       end %>
    # 
    # @example データベースから候補を取得
    #   Country.find(:all, :conditions => [...]).collect {|r| [r.name, r.id]}
    #
    # @example グループ化 ... optgroupはネストできない
    # 
    #   TODO: グループ化をどうするか
    #         1. SQL select文で一撃
    #              [opt_name, id, grp_name, grp_id], ...
    #         2. グループ名はデータベースにはないかも
    #              [opt_name, id, grp_id], ...
    #         
    def select field_name, options = {}
      raise TypeError if !field_name.is_a?(String)

      raise # TODO: impl

      field_with_error field_name, s
    end

    # input[type="submit"] or button[type="submit"]
    # イメージボタンとか作りやすいので、buttonタグを出力する
    #
    # @param [Hash] options  次のの以外はHTML inputタグの属性
    # @option options [String] :confirm => 'question?'
    # @option options :disable_submit_on_enter_key => true 名前が長い？
    def submit_tag text, options = {}
      raise TypeError if !text.is_a?(String)

      attrs = options.dup
      confirm = attrs.delete :confirm
      disable_submit = attrs.delete :disable_submit_on_enter_key

      if !disable_submit
        # 普通のサブミットボタン
        if confirm
          attrs[:onclick] = "if (!confirm('#{confirm}')) return false;"
        end
        r = start_tag "button", {:type => "submit"}.update(attrs)
      else
        # http://www.nslabs.jp/enter-submit.rhtml
        r = empty_tag "input", :type => "text", 
                   :name => "_dmy", 
                   :style => "position:absolute;visibility:hidden;"
        if confirm
          attrs[:onclick] = "if (confirm('#{confirm}')) submit();"
        else
          attrs[:onclick] = "submit();"
        end
        r << start_tag("button", {:type => "button"}.update(attrs) )
      end

      # イメージタグのこともあり
      r << h(text) << "</button>".html_safe
      return r
    end


    # エラーがあればエラーメッセージの文字列を返す
    # @param [Hash] options オプション
    # @option options [String] :class divクラス属性値
    # @option options [String] :header_message  エラーエリア冒頭に表示
    # @option options [String] :message
    def error_messages options = {}
      opt = {
        :id => "error_explanation",
        :header_message => "Please Try Again!",
        :message => "We had some problems:"
      }.update(options)

      o = @context.instance_variable_get "@#{@object_name}"
      return "" if !o

      s = ""
      if !o.errors.empty?
        s = start_tag "div", :id => opt[:id]
        s << start_tag("h2") << opt[:header_message] << end_tag("h2")
        s << start_tag("p") << opt[:message] << end_tag("p")
        
        s << start_tag("ul")
        s << o.errors.full_messages.map {|msg| 
               "<li>".html_safe + h(msg) }.join.html_safe
        s << "</ul></div>".html_safe
      end
      return s
    end


  private
    def input_tag_object_value type, field_name, options
      attrs = {
        :type => type,
        :id => id_attr(field_name),
        :name => name_attr(field_name),
      }

      if @object_name && (v = object_value(field_name)) != nil
        attrs[:value] = v.to_s
      end

      return empty_tag("input", attrs.update(options) )
    end

    def object_value field_name
      obj = @context.instance_variable_get "@#{@object_name}"
      if obj && obj.respond_to?(field_name)
        return obj.__send__ field_name
      else
        return nil
      end
    end

    def id_attr field_name
      if @object_name
        "#{@object_name}_#{field_name}"
      else
        field_name
      end
    end

    def name_attr field_name
      if @object_name
        "#{@object_name}[#{field_name}]"  # TODO: ネスト
      else
        field_name
      end
    end

    def field_with_error field_name, html
      obj = @context.instance_variable_get "@#{@object_name}"
      if obj && obj.respond_to?(:errors) && 
         obj.errors[field_name] && !obj.errors[field_name].empty?
        '<div class="field_with_errors">'.html_safe + html + '</div>'.html_safe
      else
        html
      end
    end
  end # FormBuilder

end # Wafu
