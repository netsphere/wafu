# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi, http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "rubygems"
require "active_support/dependencies/autoload"
require "erubis"   # Erubis::XmlHelper
require 'wafu2/webapp'
require "wafu2/controller_routing"
require "wafu2/form_builder"


module Wafu

  class DoubleRenderError < StandardError; end

  # サブクラス化して、サーバ側コードを書くためのクラス
  class Controller
    include CoreHelper
    include TagHelper

    class << self
      include CoreHelper

      def routes &block
        # あとで instance_variable_getされる
        @controller_routing = block
      end

      def layout layout_name, options = {}
        check_arg options, [], [:only]
        # render内でinstance_variable_getされる
        @layout_base ||= []
        @layout_base << {:name => layout_name, :options => options}
      end

      # リクエストがどのメソッドを呼び出すか分かるまではフィルタすべきか判明しないので、
      # routes内には書けない。
      # Rails3では、AbstractController::Callbacks で定義されている。
      def filter method_name, options = {}
        check_arg options, [], [:only]
        # あとで instance_variable_get する
        @filters ||= []
        @filters << {:method => method_name, :options => options}
      end
    end

    attr_reader :request
    attr_reader :response

    attr_reader :session
    attr_reader :session_options

    attr_reader :_buf

    def initialize request
      @request = request
      @session = request.session
      @session_options = request.session_options

      # postのときは authenticity_token を確認
      # ランダムなIDによるviewstateがあれば十分
      # ログインページに跳ばしたいときは、フィルタすればいい
      if @request.request_method == "POST"
        if !(vid = request["viewstate"])
          raise InvalidAuthenticityToken, "missing viewstate_id: #{vid}"
        elsif !session[vid]
          # サーバ側でセッションがexpireされた可能性もあり
          raise InvalidAuthenticityToken, "missing viewstate data"
        end

        @viewstate_id = vid
        @viewstate = session[vid]
        # @session.delete vid    ; one timeはやりすぎ
      end
    end

    def performed?
      @performed
    end

    # redirect_to, link_to などのためのURL化
    def url path, query
      raise TypeError if !path.is_a?(String)
      raise TypeError if !query.is_a?(Hash)

      r = path.dup
      if query.length > 0
        r << "?" << 
             query.collect {|k, v|
               raise ArgumentError if k =~ /[^a-zA-Z0-9_]/
               "#{k}=" + Erubis::XmlHelper.url_encode(v)
             }.join("&")
      end
      r
    end


    # Ruby on Railsのように、コントローラからrenderを呼び出せるようにする
    # bindingの制限で、ここで定義しなければならない
    # @param [Hash] options
    #     :action アクション名 [opt]
    #     :layout レイアウトファイル名 [opt] falseのときレイアウトファイルで包まない
    def render options = {}
      check_arg options, [], [:action, :layout]
      raise DoubleRenderError, 
                  "Can only render or redirect once per action" if performed?
      raise SecurityError if !request["controller"] || 
                             request["controller"] =~ /[^a-z0-9_]/
      @performed = true

      @_buf = ""
      # for content_for().
      @render_content = {}

      # まずアクションのほうからrenderする。contentなども生成
      action_name = options[:action] || request["action"]
      page_file = PACKAGE_ROOT + "/app/views/" + request["controller"] + "/" + 
                  action_name.to_s + ".html.erb"
      erb = EscapedEruby.load_file page_file, :preamble => false
      body = erb.evaluate self

      # ページを生成
      if options.has_key?(:layout) && !options[:layout]
        page = body
      else
        @_buf = ""
        layout = options[:layout] || get_layout_name(action_name.intern)
        page_file = PACKAGE_ROOT + "/app/views/_layouts/" + layout + 
                    ".html.erb"
        # load_file() はキャッシュファイルを作成する
        erb = EscapedEruby.load_file page_file, :preamble => false
        page = (erb.evaluate self do |name|
                  if name && !name.is_a?(Symbol)
                    raise TypeError, "yield name must be nil or aSymbol, but #{name.class}"
                  end
                  if name
                    @render_content[name] ? @render_content[name].html_safe : 
                                            nil
                  else
if RUBY_VERSION >= "2.0"
                    body.html_safe.force_encoding("UTF-8")
else
                    body.html_safe
end
                  end
                end)
      end

      @response = Rack::Response.new page, 200,
                                "Content-Type" => "text/html; charset=UTF-8"
      return nil
    end

  private
    # Controller.layout などからレイアウトを決定する
    def get_layout_name action
      raise TypeError if !action.is_a?(Symbol)

      layout_base = self.class.__send__(:instance_variable_get, "@layout_base")
      (layout_base || []).each do |kv|
        if kv[:options] && kv[:options][:only]
          return kv[:name] if kv[:options][:only].include?(action)
        else
          return kv[:name]
        end
      end
      return "application"
    end


    # HTTPリダイレクトを返す.
    #
    # @param [String] url リダイレクト先。
    #            railsと違い、コントローラ/アクションからURLを復元できない。
    def redirect_to url
      raise TypeError if !url.is_a?(String)
      raise DoubleRenderError, 
            "Can only render or redirect once per action" if performed?
      @performed = true

      if @viewstate_id
        session.delete @viewstate_id
        @viewstate_id = nil
        @viewstate = nil
      end

      if url.index("http:") != 0 && url.index("https:") != 0
        url = ::Application.config["app_root_url"] + url
      end

      @response = Rack::Response.new ["redirected to #{url}"], 302,
                                     "Location" => url
      return nil
    end

    def error status_code, options = {}
      raise DoubleRenderError, 
            "Can only render or redirect once per action" if performed?
      raise ArgumentError if !(status_code >= 400 && status_code <= 599)
      @performed = true

      @response = Rack::Response.new ["error"], status_code, options
      return nil
    end

    def viewstate
      @viewstate ||= {}
    end

    # 1アクションの間は同じ値を使用
    def viewstate_id
      require "digest/sha1"

      @viewstate_id ||=
        Digest::SHA1.new.update(Time.now.to_f.to_s + rand().to_s).hexdigest
    end

    # 取り出したら削除する。viewstateと違う。
    # @param [Symbol] key flashのキー. :notice または :alert
    def flash key
      raise ArgumentError if key != :notice && key != :alert

      flash = session[key] || nil
      if flash
        session.delete key
        return flash
      else
        return nil
      end
    end

    def set_flash key, value
      raise ArgumentError if key != :notice && key != :alert
      session[key] = value
    end


    # @param [Hash] options
    # @option options [String] :controller コントローラ名 (opt.)
    # 
    # @example
    #   <%= render_partial "hoge" %>
    def render_partial action_name, options = {}
      raise TypeError, "action_name must be aString" if !action_name.is_a?(String)
      raise ArgumentError if action_name =~ /[^A-Za-z0-9_]/
      check_arg options, [], [:controller]

      controller = options[:controller] || request["controller"]
      begin
        old_buffer = _buf
        @_buf = ""
        page_file = PACKAGE_ROOT + "/app/views/" + controller + 
                    "/_" + action_name + ".html.erb"
        erb = EscapedEruby.load_file page_file, :preamble => false
        body = erb.evaluate self
        return body.html_safe
      ensure
        @_buf = old_buffer
      end
    end
    private :render_partial

    # テンプレート内から呼び出し、ブロックの内容を展開して文字列として返す
    # @example
    #   <% s = capture do %>
    #   foo bar
    #   <% end %>
    def capture *args, &block
      old_buffer = _buf
      @_buf = ""
      block.call(*args)
      _buf
    ensure
      @_buf = old_buffer
    end

    # テンプレート内でブロックの内容を展開し、@render_contentに追加して格納する
    # @example
    #   <% content_for :head do %>
    #   <script>...</script>
    #   <% end %>
    def content_for name, &block
      raise TypeError if !name.is_a?(Symbol)
      @render_content[name] = (@render_content[name] || "") + capture(&block)
    end


    def form_ object_name, action_url, options, block
      builder = FormBuilder.new self, object_name

      action_url = ::Application.config["app_root_url"] + action_url if action_url[0..0] == "/"

      @_buf << 
        start_tag( "form", {:action => action_url}.update(options) ) << 
        '<input type="hidden" name="utf8" value="&#x2713;" />'.html_safe << 
        empty_tag( "input", :type => "hidden",
                                   :name => "viewstate", # 名前固定
                                   :value => viewstate_id )
      session[viewstate_id] = viewstate

      if object_name
        obj = instance_variable_get "@#{object_name}"
        if obj && obj.respond_to?("lock_version")
          @_buf << builder.hidden_field("lock_version")
        end
      end

      @_buf << capture(builder, &block)
      @_buf << "</form>"
    end
    private :form_


    # CSRF対策のため、フォームにhiddenでセッションかつワンタイムトークンを埋め込む。
    # http://www.jumperz.net/texts/csrf.htm
    # 
    # @param [Hash] options
    # @option options [Symbol] :method HTTP method. must be :get or :post
    def form action_url, options = {}, &block
      raise TypeError if !action_url.is_a?(String)
      raise ArgumentError, "missing block" if !block_given?

      form_ nil, action_url, options, block
    end
 
    # オブジェクト更新用のフォーム
    # @param [String] object_name   更新したい変数名. 先頭の"@"不要
    # @param [String] action_url
    #                 オブジェクトからURLは作れない
    # @param [Hash] options
    # @option options [Symbol] :method => :get or :post
    def form_for object_name, action_url, options = {}, &block
      raise TypeError if !object_name.is_a?(String)
      raise TypeError if !action_url.is_a?(String)
      raise ArgumentError, "missing block" if !block_given?

      form_ object_name, action_url, options, block
    end

    # <a href=...
    #
    # @param [String] url  リンク先のURL.
    #                railsと違い、"コントローラ#アクション" からURLを再構成できない。
    # @param [Hash] options 下記以外は、HTML aタグの属性になる。
    # @option options [String] :confirm => 'question?' 確認を求める
    # @option options [Symbol] :method => :get or :post
    # 
    # @return [String] HTML aタグ文字列
    def link_to text, url, options = {}
      raise TypeError if !text.is_a?(String)
      raise TypeError if !url.is_a?(String)
      raise TypeError if !options.is_a?(Hash)
      # check_arg options [], [:confirm, :method]

      attrs = options.dup
      method = attrs.delete :method
      confirm = attrs.delete :confirm

      url = ::Application.config["app_root_url"] + url if url[0..0] == "/"

      raise ArgumentError if method && (method != :get && method != :post)
      if method == :post
        # 内部でフォームを作る
        s1 = <<EOF
var f = document.createElement('form');
f.style.display = 'none';
f.method = 'POST';
f.action = '#{url}';
this.parentNode.appendChild(f);
var s = document.createElement('input');
s.setAttribute('type', 'hidden');
s.setAttribute('name', 'viewstate');
s.setAttribute('value', '#{viewstate_id}');
f.appendChild(s);
f.submit();
EOF
        # viewstate_id は Controller のメソッド.
        session[viewstate_id] = viewstate
        if confirm
          s1 = "if (confirm('" + confirm + "')) {" + s1 + "}" # エスケープ不要
        end

        # postのときはステータスバーにURLを出さない。「リンクをタブで開く」とかされる
        s = start_tag( "a", {
          :href => "#",
          :onClick => s1.gsub(/[\r\n]/, '') + "return false;"}.update(attrs) )
        # 分かってタグを書いているときはエスケープしない
        s << h(text) << "</a>".html_safe
      else 
        # method == :get or nil
        if confirm
          attrs[:onclick] = "return confirm('" + confirm + "');" # エスケープ不要
        end
        s = start_tag( "a", {:href => url}.update(attrs) )
        s << h(text) << "</a>".html_safe
      end
      
      return s
    end # link_to


    # @param [String] text リンクテキスト
    # @param [String] url  リンク先。
    #                 railsと違い、コントローラ/アクションからURLを構成できない
    # @param [Hash] options   下記以外はHTML aタグの属性
    # @option options [Hash]   :data        url宛に送信するデータ
    # @option options [String] :callback    成功したときのコールバック関数
    #
    # @example
    #   <%= link_to_replace "取得", "jq2.rb", "#videos", {:callback => << EOF} 
    #   function(text, status) {
    #     $("#videos").css("background-color", "#ffe0ff");
    #   }
    #   EOF
    def link_to_replace text, url, target, options = {}
      attrs = options.dup
      data = attrs.delete :data
      callback = attrs.delete :callback

      if !url.index("//")
        # アプリケーション内部
        data = {} if !data
        # data["authenticity_token"] = "encodeURIComponent('...')"
        data["viewstate"] = viewstate_id
        session[viewstate_id] = viewstate
      end
      if data
        os = "{" + data.collect {|k, v| "#{k}:#{v}"}.join(",") + "}"
      end
      
      # 右クリックから開くは不可
      attrs[:href] = "#"
      attrs[:onclick] = (<<EOF).gsub(/[\r\n]/, "")
$('#{target}').load('#{url}' #{data ? ", " + os : ""}
#{callback ? ", " + callback : ""});return false;
EOF

      r = start_tag "a", h
      r << h(text) << "</a>"

      return r.html_safe
    end

  end # Controller
end

