# -*- coding: utf-8 -*-


require "rubygems"

# activesupport 5.0: output_safety.rb 内で ActiveSupport::Multibyte を使ってい
# るが, require していない
# activesupport 3.0 でも 'active_support/multibyte.rb' はある。
require "active_support/multibyte"
require "active_support/core_ext/string/output_safety"
require "erubis/helper"
#require "wafu/escaped_eruby"

# このファイルだけrequireしてもよい

module Wafu
  module TagHelper
    def tag_ name, attrs = {}
      s = "<#{name}"
      if !attrs.empty? 
        # 常にエスケープする
        s << (attrs.collect do |k, v| 
                v ? " #{k}=\"#{Erubis::XmlHelper.escape_xml(v)}\"" : nil
              end.join )
      end
      s
    end
    private :tag_


    # 開始タグ
    # @param [String] name タグ名
    def start_tag name, attrs = {}
      (tag_(name, attrs) + ">").html_safe
    end


    # 空タグ
    # @param [String] name タグ名
    def empty_tag name, attrs = {}
      (tag_(name, attrs) + " />").html_safe
    end


    # 終了タグ
    def end_tag name
      "</#{name}>".html_safe
    end


    # 実行時にsafeか判定し、safeでないならHTMLエスケープする
    # @return [String] エスケープした文字列
    def h str
      raise TypeError, 
           "expected a String, but #{str.class}" if str && !str.is_a?(String)
      str.html_safe? ? str : Erubis::XmlHelper.escape_xml(str).html_safe
    end


    def raw str
      raise TypeError, 
           "expected a String, but #{str.class}" if str && !str.is_a?(String)
      str.html_safe
    end
  end # TagHelper

end
