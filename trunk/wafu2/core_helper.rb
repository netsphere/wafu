# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


require "wafu2/core_ext/symbol"


module Wafu

  module CoreHelper
    # 実引数を確認
    # このぐらいRuby組み込みでしてほしい
    def check_arg hash, req, opt
      req_ = req.dup
      hash.each {|k, v|
        raise TypeError if !k.is_a?(Symbol)
        if !req_.delete(k)
          if !opt.include?(k)
            raise ArgumentError, "key '#{k}' is wrong name."
          end
        end
      }
      raise ArgumentError, "'#{req_.first}' missing" if !req_.empty?
    end
  end # CoreHelper

end # Wafu
