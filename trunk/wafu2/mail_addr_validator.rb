# -*- coding: utf-8 -*-

# ActiveRecordにメールアドレス検査を追加

require "rubygems"
require "active_model/validator"

module Wafu
  # メールアドレス検証
  class MailAddrValidator < ActiveModel::EachValidator
    MAIL_ADDR_PATTERN = /\A((?:[A-Za-z0-9!#$%&'*+\-\/=?^_`{|}~]{1,64}(?:\.[A-Za-z0-9!#$%&'*+\-\/=?^_`{|}~]{1,62}){0,31}|"(?:[\x21\x23-\x5B\x5D-\x7E]|\\[\x20-~]){0,62}"))@((?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])?\.){0,127}[A-Za-z](?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])?)|(?:\[(?:(?:[0-9]{1,3}(?:\.[0-9]{1,3}){3})|(?:(?:[0-9A-Fa-f]{1,4})?(?:(?:::|:)(?:[0-9A-Fa-f]{1,4})){1,7}(?:(?:::|:)(?:[0-9]{1,3}(?:\.[0-9]{1,3}){3}))?))\]))\z/
    
    # @override
    def check_validity!
      # オプションの検査
    end

    def validate_each record, attribute, value
      record.errors[attribute] = "妥当ではありません。" if MAIL_ADDR_PATTERN !~ value.to_s
    end
  end # class MailAddrValidator

end


class << ActiveRecord::Base
  def validates_mail_addr_of *attr_names
    validates_with Wafu::MailAddrValidator, _merge_attributes(attr_names)
  end
end

