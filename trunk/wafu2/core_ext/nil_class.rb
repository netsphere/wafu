# -*- coding: utf-8 -*-


class NilClass # :nodoc:
  # Raises a RuntimeError when you attempt to call +id+ on +nil+.
  # nil.id => 4 なので、うっかり呼び出すと誤動作の元。
  # ActiveRecord対策。
  def id
    raise RuntimeError, "Called id for nil -- if you really wanted the id of nil, use object_id", caller
  end
end

