# -*- coding:utf-8 -*-

# 本物のERBを読み込むのは無駄. erubisに差し替え

require "erubis"

class ERB
  module Util
    def html_escape s
      raise "dmy method"
    end
    alias h html_escape
    module_function :h
    module_function :html_escape
  end

  def initialize template, safe_level = nil, trim_mode = nil
    @erb = Erubis::Eruby.new template
    self
  end

  def result context = TOPLEVEL_BINDING
    @erb.result context
  end
end

