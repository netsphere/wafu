# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


require "rubygems"
#require "wafu2/session"
require "active_record"


module Wafu
  module Session

    # テーブル
    #   id 
    #   session_id  文字列 unique
    #   data        文字列 長さ4k
    #   accessed_at timestamp
    #   
    class ActiveRecordStorage
      include CoreHelper

      # @param [Hash] options
      def initialize options
        check_arg options, [:table_class, :holdtime], []
        raise TypeError if !options[:table_class].is_a?(Class)

        @ar_class = options[:table_class]
        @holdtime = options[:holdtime] # 単位は秒
      end

      # @param [String] session_id
      # @return [Hash] セッションデータ。セッション情報がない場合はnil
      def fetch session_id
        raise TypeError if !session_id.is_a?(String)

        @ar_class.transaction do
          r = @ar_class.find_by_session_id session_id
          if r
            Marshal.load(r.data)
            r.accessed_at = Time.now
            r.save!
          else
            nil
          end
        end
      end

      def store session_id, session_data
        raise TypeError if !session_id.is_a?(String)
        raise TypeError if !session_data.is_a?(Hash)

        @ar_class.transaction do 
          r = @ar_class.find_by_session_id(session_id) || 
              @ar_class.new(:session_id => session_id)
          r.data = Marshal.dump session_data
          r.accessed_at = Time.now
          r.save!
        end
      end

      def delete session_id
        raise TypeError if !session_id.is_a?(String)

        @ar_class.delete_all ["session_id = ?", session_id]
      end

      def sweep
        @ar_class.delete__ :conditions => [
                                 "accessed_at < ?", Time.now - @holdtime]
      end
    end

  end # Session
end # Wafu
