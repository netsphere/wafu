# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


# require "wafu2/session"
require "digest/md5"  # CGI互換のため
# require "erubis/helper" # url_encode()
require "wafu2/core_helper"


module Wafu
  module Session

    # File-based session storage class.
    #
    # Wafu::SessionManager または Wafu::Session::ServerSessionStore (Rails3) 
    # と組み合わせる
    #
    # CGI::Session::FileStore とファイルの内容に互換性を持たせる
    #
    class FileStorage
      include CoreHelper

=begin
      class << self
        # Erubisにはurl_encode()の逆関数がない。なぜ？
        def unescape string
          string.tr('+', ' ').gsub(/((?:%[0-9a-fA-F]{2})+)/n) do
            [$1.delete('%')].pack('H*')
          end
        end
      end # << self
=end

      # @param [Hash] options
      # @option options [String] :dir ファイルを保存するディレクトリ
      # @option options [Integer] :holdtime  セッション維持時間. 単位は秒
      def initialize options
        check_arg options, [:dir, :holdtime], [:prefix, :suffix]
        raise TypeError if !options[:dir].is_a?(String)
        if options[:dir].length == 0 || options[:dir][0..0] != "/" || 
           options[:dir][-1..-1] == "/" 
          # rootは不可. 末尾"/"は不要
          raise ArgumentError 
        end
        raise ArgumentError, "internal error: missing dir #{options[:dir]}" if !FileTest.directory?(options[:dir])
        
        raise TypeError if !options[:holdtime].is_a?(Integer)

        @options = options
      end


      # セッション情報を得る
      # @param [String] session_id  セッションID
      # @return [Hash] セッションデータ。セッション情報がない場合はnil
      #
      # CGI::Session::FileStore は .lockファイルの使い方が変態.
      # [ruby-dev:32296] で入った修正 (Ruby 1.8.6).
      #
      def fetch session_id
        raise TypeError if !session_id.is_a?(String)
        raise SecurityError if session_id =~ /[^0-9A-Za-z]/

        fname = session_filename session_id
        hash = {}
        begin
          # ファイルシステムがnoatimeでマウントされている可能性があるため、
          # atimeは使えない。
          # posix utime() は, 現在の時刻以外に変更しようとすると, EPERM を発生させる
          # ことがある => 使えない
          # now = Time.now
          # File.utime now, now, fname   # mtimeを更新

          # ロックしない
          File.open fname, "r+", 0666 do |f|
=begin
            # CGI互換
            for line in f
              line.chomp!
              k, v = line.split("=", 2)
              hash[FileStorage.unescape(k)] = FileStorage.unescape(v)
            end
=end
            hash = Marshal.load f
          end
        rescue Errno::ENOENT
          # sweepされている
          return nil
        end
        return hash
      end


      # セッション情報を保存する
      def store session_id, session_data
        raise TypeError if !session_id.is_a?(String)
        raise TypeError if !session_data.is_a?(Hash)
        raise SecurityError if session_id =~ /[^0-9A-Za-z]/

        # ロックしない. uniqueなファイルに出力してrenameする
        # CGI::Session::FileStore は固定ファイル (.new) に出力しているため、
        # ロックが必要になっている。
        fname = @options[:dir] + "/" + make_tmpname
        File.open fname, "w", 0666 do |f|
=begin
          # CGI互換
          for k, v in session_data
            f.print Erubis::XmlHelper.url_encode(k), "=",
                    Erubis::XmlHelper.url_encode(v), "\n"
          end
=end
          Marshal.dump session_data, f
        end
        File.rename fname, session_filename(session_id)
      end


      # セッション情報を削除する
      def delete session_id
        raise TypeError if !session_id.is_a?(String)
        raise SecurityError if session_id =~ /[^0-9A-Za-z]/

        begin
          File.unlink session_filename(session_id)
        rescue Errno::ENOENT
          # sweepされている
        end
      end


      # 一定時間アクセスのないサーバ側セッション情報を削除
      def sweep
        now = Time.now
        Dir.glob @options[:dir] + "/*" do |fname|
          begin
            File.unlink fname if File.mtime(fname) < now - @options[:holdtime]
          rescue Errno::ENOENT
            # 並行プロセスが削除ずみ
          end
        end
      end


      private

      def session_filename session_id
        md5 = Digest::MD5.hexdigest(session_id)[0, 16] # CGI互換
        return @options[:dir] + "/" + (@options[:prefix]||"") + md5 + 
                                                      (@options[:suffix]||"")
      end


      # ユニークなファイル名を作る
      def make_tmpname
        md5 = Digest::MD5.new
        now = Time.now
        md5.update(now.to_s)
        md5.update(String(now.usec))
        md5.update(String(rand(0)))
        md5.update(String(Process.pid))
        return md5.hexdigest + ".tmp"
      end
    end # class FileStorage

  end # module Session
end # Wafu
