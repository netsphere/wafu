# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


require "wafu2/core_helper"

require "rubygems"
# Ruby組み込みのSecureRandom が使える場合はそれ、使えない場合は互換インターフェイス
require "active_support/secure_random" 


# Marshal.load() 対策。いちいちrequireすると遅い
module OpenID
  class Consumer
    class DiscoveredServices; end
  end
  class OpenIDServiceEndpoint; end
end

# action_controller/metal/request_forgery_protection.rb
module ActionController
  class ActionControllerError < StandardError; end
  class InvalidAuthenticityToken < ActionControllerError; end
end


module Wafu
  module Session

    # Rack::Session::Cookie は簡単だが、グローバルな secret key が漏れると
    # セッション情報が作りたい放題になってしまう。
    # クライアントにはセッションキーのみ発行し、セッション情報そのものはサーバ
    # で持つ.
    #
    # ActionDispatch::Session::AbstractStore と同じ作りにする.
    #
    class SessionManager
      include CoreHelper

      # @param [Hash] options
      # @option options [Hash] :database_manager  
      #                 セッションデータを保存するクラスとそのオプション
      #     @option :database_manager [Class] :class   クラス
      #     @option :database_manager その他    クラスごとに異なるオプション
      def initialize app, options = {}
        check_arg options, [:database_manager], 
            [:key, :expire_after,
             :domain, :path, :secure, :httponly]

        raise TypeError if !options[:database_manager].is_a?(Hash)
        check_arg options[:database_manager], [:class], [
                   :table_class,   # AR
                   :dir, :holdtime, :prefix, :suffix # FileStore
                 ]

        @app = app
        @key = options[:key] || "_session_id"

        dboptions = options.delete :database_manager
        @dbman = (dboptions.delete :class).new dboptions

        @session_options = options.merge :cookie_only => true
      end


      def call env
        load_session env
        status, headers, body = @app.call env
        commit_session env, status, headers, body
      end


  private
      # session fixation (セッション固定, 強要) 攻撃を受けるため、クエリ等URLから
      # セッションIDを取得してはならない。
      # 古いケータイではクッキーが使えないが、やむを得ない。
      #   => envオプションでセッションIDを与えられないようにする
      def load_session env
        request = Rack::Request.new env

        # 古いセッション情報を削除
        @dbman.sweep

        @session_id = request.cookies[@key]
        if !@session_id
          @session_id = generate_sid
          session_data = {}
        else 
          session_data = @dbman.fetch @session_id
          if !session_data
            # セッション情報がない。サーバ側でexpireした場合
            # ただし、セッションIDは攻撃者が決めたものかもしれない。セッションIDを取り直す
            @session_id = generate_sid
            session_data = {}
          end
        end

        env["rack.session"] = session_data
        env["rack.session.options"] = @session_options.dup
      end


      # @param [Hash] env
      # @option env [Hash] "rack.session"  セッションデータ
      # @option env [Hash] "rack.session.options"  オプション
      #        :renew   trueの場合、セッションIDを取り直し、そのIDでデータを保存
      #                 古いセッション情報は破棄
      def commit_session env, status, headers, body
        session_data = env["rack.session"]
        options = env["rack.session.options"]

        if options[:renew]
          @dbman.delete @session_id  # これ重要
          session_id = generate_sid
          options.delete :renew
        else
          session_id = @session_id
        end

        @dbman.store session_id, session_data

        cookie = {
          :value => session_id,
          :domain => options[:domain],
          :path => options[:path] ||  
                 (env["SCRIPT_NAME"] && File.dirname(env["SCRIPT_NAME"]) != "." ? File.dirname(env["SCRIPT_NAME"]) : nil),
          :secure => options[:secure],
          :httponly => options[:httponly] # IE独自？
        }
        if options[:expire_after]
          cookie[:expires] = Time.now + options[:expire_after] 
        end

        ::Rack::Utils.set_cookie_header! headers, @key, cookie

        [status, headers, body]
      end


      # 新しいセッションID
      def generate_sid
        return ActiveSupport::SecureRandom.hex(16)
      end

    end # class SessionManager

  end # module Session
end # module Wafu
