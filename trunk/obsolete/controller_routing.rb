# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


module Wafu

  class ControllerRouting
    include CoreHelper
    include RoutingHelper

    # 順序が大切
    DEFAULT_MAP = [
      # http_method, pattern, action_name
      [:get,  "",            :index   ],
      [:get,  "new",         :new     ],
      [:post, "create",      :create  ],
      [:get,  ":id/",        :show    ], # newなどより後ろ
      [:get,  ":id/edit",    :edit    ],
      [:post, ":id/update",  :update  ],
      [:post, ":id/destroy", :destroy ]
    ].freeze

    # よくあるスタイルを設定. RESTfulっぽく
    def default_methods options = {}
      check_arg options, [], [:only]

      actions = filter_actions options[:only]
      actions.each do |act|
        # raise act.inspect # DEBUG
        map act[0], act[1], act[2]
      end
      nil
    end

    # 細かく設定
    # action_nameを省略したときは、patternをメソッド名とする
    def map http_method, pattern, action_name = nil
      raise ArgumentError if http_method != :get && http_method != :post
      raise TypeError if !pattern.is_a?(String)
      raise TypeError if action_name && !action_name.is_a?(Symbol)

      pos = @scanner.pos

      if pattern[-1..-1] == "/"
        kv = match_pattern("/" + pattern[0..-2], @scanner)

        if kv 
          # URLの末尾"/"省略は救っておく
          if !@scanner.eos?
=begin
            response = Rack::Response.new ["redirected"], 302, 
                       "Location" => ::Application.config["app_root_url"] +
                                     @request.path_info + "/"
            throw :response, response
          else
=end
            @scanner.getbyte
          end
        end
      else
        # 通常パターン
        kv = match_pattern("/" + pattern, @scanner)
      end

      # 後ろにパスが続くときもunmatch
      if !kv || !@scanner.eos?
        @scanner.pos = pos
        return
      end

      # HTTPメソッドが違うときは不可. not foundにはしない。
      if http_method == :get && @request.request_method != "GET" ||
           http_method == :post && @request.request_method != "POST" 
        response = Rack::Response.new(["method not allowed"], 405)
        throw :response, response
      end

      kv.each {|k, v| @request[k] = v }
      @request["action"] = action_name || pattern
      @request["action"] = @request["action"].intern if !@request["action"].is_a?(Symbol)

      controller = @controller_class.new @request

      # クラス内のフィルタ
      filters = @controller_class.__send__ :instance_variable_get, "@filters"
      if filters 
        action_invoke = lambda do controller.__send__ @request["action"] end

        aiv = filters.inject(action_invoke) do |memo, filter|
          if filter[:options][:only] && 
                !filter[:options][:only].include?(@request["action"])
            memo 
          else
            lambda do controller.__send__ filter[:method], memo end
          end
        end
        aiv.call
      else
        # コントローラのメソッドを呼び出す
        controller.__send__ @request["action"]
      end

      if !controller.performed?
        controller.render
      end
      throw :response, controller.response
    end


    def initialize request, scanner, controller_class
      raise TypeError if !controller_class.is_a?(Class)

      @request = request
      @scanner = scanner
      @controller_class = controller_class
    end

  private
    # :only オプション
    # 指定がないときは全部
    def filter_actions ary
      raise TypeError if ary && !ary.is_a?(Array)

      return DEFAULT_MAP if !ary

      actions = ary.dup
      r = []
      DEFAULT_MAP.each do |line|
        if (i = actions.index line[2]) != nil
          r << line
          actions.delete_at i
        end
      end
      raise ArgumentError, 
                    "unknown action: #{actions.to_s}" if !actions.empty?

      return r
    end
  end

end # Wafu
