
# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


require "rubygems"
require "wafu2/webapp"

env = ENV["RACK_ENV"]
raise ArgumentError, "missing environment" if !env
raise ArgumentError if env =~ /[^a-z0-9_]/

raise ArgumentError if !PACKAGE_ROOT
$LOAD_PATH << PACKAGE_ROOT + "/app"

require PACKAGE_ROOT + "/config/" + env
config_class = Object.const_get("Config").const_get(env.camelize)

require "application"
::Application.config = config_class.__send__(:class_variable_get, "@@hash")
