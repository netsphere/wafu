# -*- coding: utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
# http://www.nslabs.jp/


require "rubygems"
gem "activesupport"
require "strscan"
require "active_support/core_ext"  # String#camelize()
require "wafu2/core_helper"


module Wafu

  module RoutingHelper
    # マッチするか調べる
    # パターン"/foo" は URL "/foobar" にはマッチしない（ディレクトリの区切りを確認する）
    #
    # @param [String] pattern マッチするか調べるパターン
    def match_pattern pattern, scanner
      raise TypeError if !pattern.is_a?(String)

      ary = divide pattern
      pos = scanner.pos
      kv = {}
      ary.each {|pat|
        if pat[0..0] != ":"
          # placeholder以外
          if !scanner.scan(/#{Regexp.quote(pat)}/)
            scanner.pos = pos
            return nil
          end
        else
          if (matched = scanner.scan(/[A-Za-z0-9_-]+/)) != nil
            kv[pat[1..-1]] = matched
          else
            scanner.pos = pos
            return nil
          end
        end
      }

      if !scanner.eos? && scanner.peek(1) != "/"
        scanner.pos = pos
        return nil # matchせず
      end
        
      return kv
    end

  private
    # placeholder とそうでない部分に分ける
    def divide pattern
      ary = []
      while /\:[A-Za-z0-9_]+/ =~ pattern
        ary << $` if $` != ""
        ary << $&
        pattern = $'
      end
      ary << pattern if pattern != ""
      return ary
    end

  end # RoutingHelper


  # アプリケーションで指定する、コントローラへのルーティング
  class AppRouting
    include CoreHelper
    include RoutingHelper

    # マッチした場合コントローラを呼び出す
    # URLに末尾の"/"がないとき、redirectする。
    # マッチしないときはスルー
    #
    # @param [Hash] options
    #     :controller コントローラ名 (全部小文字、"_"でつなぐ, "Controller"は付けない)
    #     :falldown   コントローラでURLがマッチしなかったとき呼び出すブロック
    def resource name, options = {}
      check_arg options, [], [:controller, :falldown]
      raise TypeError if !name.is_a?(String)
      raise SecurityError if name =~ /[^A-Za-z0-9_]/   # "-"は不可

      pos = @scanner.pos
      if @scanner.scan(/\/#{name}/)
        if @scanner.eos?
          response = Rack::Response.new(["redirected"], 302, 
                       "Location" => ::Application.config["app_root_url"] + 
                                     @request.path_info + "/")
          throw :response, response
        end
        if @scanner.peek(1) != "/"
          @scanner.pos = pos
          return  # matchせず
        end

        # matchした。コントローラのroutingに回す
        response = call_controller_routes(options[:controller] || name)

        if !response
          # コントローラがスルーした場合はfalldownに回す
          if options[:falldown]
            response = catch(:response) {
              options[:falldown].call
            }
          end
          
          response = Rack::Response.new(["resource: Not found"], 404) if !response
        end

        throw :response, response
      end
    end

    # patternにmatchした場合、ブロックを評価
    # マッチしないときはスルー
    # @param [Hash] options
    #          :filter マッチしブロックを評価する前にフィルタを呼び出す
    def match pattern, options = {}, &block
      # check_arg options, [], [:filter]
      raise TypeError if !pattern.is_a?(String)
      raise ArgumentError, "missing block" if !block_given?

      if (kv = match_pattern(pattern, @scanner)) != nil
        if @scanner.eos? && pattern[-1..-1] != "/"
          response = Rack::Response.new(["redirected"], 302,
                                        "Location" => @request.path_info + "/")
          throw :response, response
        end

        kv.each {|k, v| @request[k] = v }
=begin
        if options[:filter]
          raise TypeError if !options[:filter].is_a?(String)

          filter = Object.const_get(options[:filter]).new block
          response = filter.call @request
        else
=end
          # フィルタがないときは、単にブロックに回す
          response = catch(:response) {
            yield
          }
          response = Rack::Response.new(["match: Not found"], 404) if !response
        # end
        throw :response, response
      end
    end


    # 単純に、全部コントローラへ振る
    def default_controller cntlr_name
      raise TypeError if !cntlr_name.is_a?(String)
      raise SecurityError if cntlr_name =~ /[^A-Za-z0-9_]/   # "-"は不可

      response = call_controller_routes cntlr_name
      throw :response, response
    end

    def initialize request
      raise SecurityError, "path_info missing" if request.path_info[0..0] != "/"
      @request = request
      @scanner = StringScanner.new request.path_info
    end

  private
    # @param [String] cntlr_name コントローラ名 (クラス名ではない)
    def call_controller_routes cntlr_name
      require cntlr_name + "_controller"
      cntlr_class = Object.const_get(cntlr_name.camelize + "Controller")
      cntlr_routes = cntlr_class.__send__ :instance_variable_get, 
                                      "@controller_routing"
      @request["controller"] = cntlr_name
      pos = @scanner.pos
      cr = ControllerRouting.new @request, @scanner, cntlr_class

      response = catch(:response) {
        cr.instance_eval &cntlr_routes 
      }
      @scanner.pos = pos if !response

      return response
    end

  end # AppRouting


end # Wafu
