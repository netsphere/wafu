# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# このライブラリを使っているプロジェクト：
#    Q's Web Account Book
#    Q's WebDiary2
#    common/wa-common.rb
#    Q's FotoAlbum


require 'rubygems'
require 'rack'
# require 'wafu2/mail-injector.rb' # send_mail()
require 'wafu2/app_routing'
require "active_record"
require "wafu2/core_ext/nil_class"


# Web Application Framework 'U'
module Wafu  

  # セッション管理と、アクションのルーティングを担当
  class WebApp

    class << self
      # ルーティングを設定
      def routes &block
        @@app_routing = block
      end

      # 設定を参照
      attr_accessor :config

      # rack middlewareを起動
      def middleware builder
        builder.instance_eval &@config["middleware"]
      end
    end

    def initialize
      ActiveRecord::Base.establish_connection self.class.config["database"]
    end

    # 実行する。
    # URLで決定される controller, action を起動する
    def call env
      request = Rack::Request.new env

      # CGIだし、キャッシュなどは保存しない
      routes = AppRouting.new request
      response = catch(:response) {
        routes.instance_eval &@@app_routing
      }

      if !response
        # 何にもmatchしなかった
        response = Rack::Response.new(["call: Not found"], 404)
      end

      if response.status >= 400 && File.exist?(PACKAGE_ROOT + "/app/views/#{response.status}.html")
        response.body = [File.read(PACKAGE_ROOT + "/app/views/#{response.status}.html")]
      end 

      return response.finish
    end

=begin
    # TODO: フィルタへ移動
    def internal_error_message_out()
      print <<EOF
Content-Type:text/html

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
An exception raised.
<p>#{CGI.escapeHTML($!.inspect)}
<p>
EOF
      $@.each {|x| print CGI.escapeHTML(x), "<br>\n"}
      print <<EOF
<hr>
#{CGI.escapeHTML(cgi.params.inspect)}
</body>
</html>
EOF
    end

  private
    # TODO: フィルタへ移動
    def send_error_mail(err, at)
      raise RuntimeError if !AppConfig[:admin_mail]

      site_name = AppConfig[:site_name]
      header = {
        "From" => AppConfig[:prog_mail],
        "To" => AppConfig[:admin_mail],
        "Subject" => "#{site_name}: エラー発生",
        "MIME-Version" => "1.0",
        "Content-Type" => "text/plain; charset=ISO-2022-JP"}
      mailbody = <<EOF
#{site_name}: エラー発生

<date>#{Time.now}</date>

<userid>
#{@user.inspect}
</userid>

<ua>
#{ENV["HTTP_USER_AGENT"]}
</ua>

<message>
#{err.inspect}
</message>

<backtrace>
EOF
      at.each {|x| mailbody += x + "\n"}
      mailbody << "</backtrace>\n"
      mailbody << "<request>\n" << cgi.params.inspect << "</request>\n"

      send_mail(header, mailbody)
    end
=end
  end # class WebApp

end # module Wafu

