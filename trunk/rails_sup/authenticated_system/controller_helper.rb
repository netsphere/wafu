# -*- coding:utf-8 -*-


# https://rubygems.org/gems/restful_authentication と
# https://rubygems.org/gems/restful-authentication とがある.
# => どちらもリンク切れ。ひどい.

# 生きているのは、ここ (from ruby-toolbox.com); 
# https://github.com/technoweenie/restful-authentication
# => ただし、これでも 7年前に止まっている.


module AuthenticatedSystem

  # ApplicationController に include させる
  module ControllerHelper
    protected

    # Inclusion hook to make #current_user and #logged_in?
    # available as ActionView helper methods.
    def self.included(base)
      base.send :helper_method, :current_user, :logged_in?
    end

    
    # ActionView helper method
    def current_user
      return @current_user if @current_user

      if !session[:user_login].blank?
        # constantize は ActiveSupport::Inflector class.
        # Rails 2.2 から 4.2 まで対応
        kls = UserModel.to_s.constantize
        new_user = kls.find_by_login(session[:user_login])
        if new_user
          only_set_current_user new_user
        else
          session[:user_login] = nil
        end
      #else
      #  login_from_cookie
      end
      return @current_user
    end

    
    # login. アクセサではない (メソッド名と違う).
    def set_current_user new_user, permanent = false
      only_set_current_user new_user
      remember_user permanent
      # SSO リセット
      cookies.delete :sso_auth_token,
                     :domain => Application.config.service_domain
      
      return self
    end


    # 内部メソッド
    # @private
    def only_set_current_user new_user 
      if new_user.class.to_s != UserModel.to_s
        raise TypeError,
              "new_user must be a #{UserModel}, but #{new_user.inspect}"
      end

      # ゲスト => ユーザのときはリセットしないことに注意.
      if @current_user && @current_user.login != new_user.login
        reset_session() 
      else
        session.options[:renew] = true  # Session ID のみ差替え
      end
      @current_user = new_user
      session[:user_login] = new_user.login
    end


    # @private
    def clear_auth_token
      if AuthTokenModel
        if cookies[:auth_token]
          AuthTokenModel.delete_all ["remember_token = ? OR expires_at < ?",
                                     cookies[:auth_token],
                                     Time.now - 60 * 10] # 10分
          cookies.delete :auth_token
        end
      end
    end
    private :clear_auth_token

    
    def logout_user
      # @current_user が設定される前、のことがある.
      if !session[:user_login].blank? || @current_user
        session[:user_login] = nil
        @current_user = nil
        reset_session()
      end
      clear_auth_token
      return self
    end


    # "remember me"機能. before_filterに指定する
    def login_from_cookie
      # SSO の機能だが, ここに入れるしかない
      if !cookies[:sso_auth_token].blank? && cookies[:sso_auth_token] == "AUTH_LOGOUT"
        logout_user()
        return
      end
      
      if AuthTokenModel
        return if cookies[:auth_token].blank?

        kls = AuthTokenModel.to_s.constantize
        token = kls.find_by_remember_token(cookies[:auth_token])
        if token && token.valid_remember_token? && token.user
          only_set_current_user token.user
        else
          clear_auth_token
        end
      end
    end


    # ActionView helper method
    # この中で current_user の更新も行う.
    def logged_in?
      return !current_user.nil?
    end


    # "remember me" を有功にする
    def remember_user permanent
      if !@current_user
        raise "internal error: can remember_user() only if logged-in." 
      end
    
      if AuthTokenModel
        clear_auth_token

        if permanent
          token = AuthTokenModel.create_token_for current_user
          v = { 
            :value => token.remember_token,
            :expires => token.expires_at
          }
          cookies[:auth_token] = v
        end
      end
    end


    # for before_filter()
    def login_required
      if !logged_in?
        redirect_to_login
      end
    end
    private :login_required
  

    # must be overrided
    def redirect_to_login
      raise "must be overrided."
    end
    private :redirect_to_login

    
    def redirect_back_or_default default
      if default.is_a?(String) &&
         ((%r<\Ahttp(?:s)?://> =~ default) == 0 || default[0..0] == '/')
        # ok
      elsif default.is_a?(Hash)
        # ok
      else
        raise TypeError, "must be Hash or String, but #{default.class}"
      end

      puts ":return_to = #{session[:return_to]}" # DEBUG
      
      if session[:return_to].is_a?(URI::HTTP) || # HTTPSはHTTPの派生クラス
         session[:return_to].is_a?(URI::Generic)
        redirect_to session[:return_to].to_s
      elsif !session[:return_to].blank?
        redirect_to session[:return_to]
      else
        redirect_to default
      end

      session[:return_to] = nil
    end
  
  end # module ControllerHelper
end
