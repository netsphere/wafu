# -*- coding:utf-8 -*-

# SSO の実現方法 (とても手抜き).

module AuthenticatedSystem
  module Client
    
    # プロバイダとクライアントでtoken を検証するために, テーブルを共有する方法.
    # 設定:
    #   SsoAuthTokenModel remember_token フィールド
    #                  valid_remember_token? メソッド
    #                  create_token_for(user) メソッド
    module SharedAuthToken
      # token はごく短い時間しか有効でない.
      # @return [String] login
      def self.verify_token token_str, client_key
        raise TypeError if !token_str.is_a?(String)
        raise TypeError if !client_key.is_a?(String)
      
        token = SsoAuthTokenModel.find_by_remember_token token_str
        if !token || token.user_login.blank? || !token.valid_remember_token?
          return nil
        end
        if client_key != token.client_key  # 対象アプリが違う or トークン置換攻撃
          return nil # raise SecurityError
        end
        
        return token.user_login
      end
    
    end # module SharedAuthToken

  end # module Client
end
