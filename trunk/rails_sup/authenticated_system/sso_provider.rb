# -*- coding:utf-8 -*-

module AuthenticatedSystem

  # Id Provider の AccountController などに include させる
  module SsoProvider

    def set_sso_access_token user, client_key
      raise TypeError if !user
      raise TypeError if !client_key.is_a?(String)
      
      token = AuthTokenModel.create_token_for user, client_key
      v = {
        :value => token.remember_token,
        :domain => Application.config.service_domain
      }
      cookies[:sso_auth_token] = v
    end

    
    def sso_logout
      v = {
        :value => "AUTH_LOGOUT",
        :domain => Application.config.service_domain
      }
      cookies[:sso_auth_token] = v
    end
  end # module SsoProvider
  
end

