# -*- coding: utf-8 -*-

require "rubygems"
require 'active_support/core_ext/hash/keys'
require 'active_support/core_ext/object/blank'

# actionpackパッケージに依存
require "action_dispatch/middleware/session/abstract_store"


module Wafu
  module Session

    # 機能は Wafu::Session::SessionManager とまったく同じ
    # 
    # Rails ヴァージョンで, 基底クラス名が変更になっている.
    # actionpack-4.2.11.3/lib/action_dispatch/middleware/session/abstract_store.rb
    #     class AbstractStore < Rack::Session::Abstract::ID
    #
    # actionpack-5.2.8/lib/action_dispatch/middleware/session/abstract_store.rb
    # actionpack-7.0.2.4/lib/action_dispatch/middleware/session/abstract_store.rb
    #     class AbstractStore < Rack::Session::Abstract::Persisted
    #
    # Rack::Session::Abstract::ID で定義されるメソッドのオーバーライドが必要.
    #
    class RailsSessionManager < ActionDispatch::Session::AbstractStore

      # @param [Hash] options 各種オプション
      # @option options [Hash] :database_manager
      #                 セッションデータを保存するクラス名とそのオプション
      #
      def initialize app, options = {}
        dboptions = options.delete :database_manager
        raise ArgumentError, "need :database_manager option" if !dboptions

        # クエリなどのURLからセッションIDを取得しない
        super app, options.merge(:cookie_only => true)
        @pool = (dboptions.delete :class).new dboptions

        freeze
      end


    private

      # @callback
      def get_session env, sid
        # 古いセッション情報を削除
        @pool.sweep

        sid ||= generate_sid
        session_data = @pool.fetch sid
        if !session_data
          # セッション情報が見つからない。
          # expireしたか、攻撃者がセッションIDを与えたのかもしれない。
          # セッションIDを取り直す
          sid = generate_sid
          session_data = {}
        end

        return [sid, session_data]
      end


      # @callback
      # rack 1.4 は options が増えた.
      def set_session env, sid, session_data, options = nil
        options = env[ENV_SESSION_OPTIONS_KEY] if !options
        if options[:renew]
          # ログイン/ログアウト時にはセッションIDを振りなおす
          @pool.delete sid
          sid = generate_sid
if Rails::VERSION::STRING >= "4.0"
          options[:renew] = nil
else
          options.delete :renew
end
        end

        @pool.store sid, session_data
        return sid
      end


      # @callback
      # rack 1.4 はメソッド名が違う. sid, options が増えた
      def destroy_session env, sid, options
        @pool.delete sid
        generate_sid
      end


      ###############################
      # Rails 5...7 で基底クラスが Rack::Session::Abstract::Persisted に変更に
      # なったことに対する対応.

      def find_session(req, sid)
        get_session req.env, sid
      end

      def write_session(req, sid, session, options)
        set_session req.env, sid, session, options
      end

      def delete_session(req, sid, options)
        destroy_session req.env, sid, options
      end

=begin
      # for actionpack 3.0.x.
      def destroy env
        if (sid = current_session_id(env)) != nil
          @pool.delete sid
        end
        generate_sid
      end
=end
      
    end # class RailsSessionManager

  end # module Session
end # module Wafu
