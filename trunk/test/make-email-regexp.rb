# -*- coding:utf-8 -*-

# メイルアドレスの正規表現を作る

# RFC 5321 Simple Mail Transfer Protocol (October 2008)
# ※ RFC 5322 ではない
#   Mailbox        = Local-part "@" ( Domain / address-literal )


# Email Address test cases
# <s>https://blogs.msdn.microsoft.com/testing123/2009/02/06/email-address-test-cases/</s>
# -> https://learn.microsoft.com/en-us/archive/blogs/testing123/email-address-test-cases

# https://www.rfc-editor.org/rfc/rfc3696
# Application Techniques for Checking and Transformation of Names
#     email address, URL & URI

# 正規表現メモ
#   (?:  部分文字列の参照をしない. キャプチャしない括弧.
#   (?-mix: オプション. /cats/i => (?i-mx:cats)

# RFC 5952 A Recommendation for IPv6 Address Text Representation (Aug 2010)
#   2001:db8::1
#   2001:db8::2:1
#   IPv4-Mapped IPv6 address: ::ffff:192.0.2.1
#   ::1

# 挙動確認, ReDoS チェック
# https://regex101.com/
# recheck https://makenowjust-labs.github.io/recheck/playground/


##################################
# local-part

# タブ文字は除外. 空白, ',', '"', '\' も書ける.
quoted_pair = %Q<\\\\[\\x20-~]>  # ("\" (VCHAR / WSP)) 

atext = %Q<[A-Za-z0-9!#\$%&'*+\\-/=?^_`{|}~]>
dot_string = %Q<#{atext}{1,64}(?:\\.#{atext}{1,62}){0,31}> 

qtext = %Q<[\\x21\\x23-\\x5B\\x5D-\\x7E]>    # '"' と '\' 以外.
qcontent = %Q<(?:#{qtext}|#{quoted_pair})>
quoted_string = %Q<"#{qcontent}{0,62}">

# <var>obs-local-part</var> は除外.
# local-part は 64オクテットまで. (RFC 5321 section 4.5.3)
local_part = %Q<(?:#{dot_string}|#{quoted_string})>

##################################
# address-literal

# <var>domain-literal</var> を有効にする場合. <var>obs-domain</var> は除外.
dtext = %Q<[\\x21-\\x5A\\x5E-\\x7E]>  # 次を除く: '[' '\' ']'

# 値の範囲まで見ていない
ipv4_address = %Q<(?:[0-9]{1,3}(?:\\.[0-9]{1,3}){3})>

ipv6_hex = %Q<(?:[0-9A-Fa-f]{1,4})>

# 簡略化している
# ::ffff:192.0.2.1
# ::1
ipv6_address = %Q<(?:#{ipv6_hex}?(?:(?:::|:)#{ipv6_hex}){1,7}(?:(?:::|:)#{ipv4_address})?)>

# RFC 5321 の定義.
ldh_str = %Q<(?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])>

# https://www.iana.org/assignments/address-literal-tags/
# 2017.7.25 これはやりすぎ。外す.
#general_address = %Q<(?:[a-zA-Z]#{ldh_str}?:#{dtext}{1,61})>

#address_literal = %Q<(?:\\[(?:#{ipv4_address}|#{ipv6_address}|#{general_address})\\])>
address_literal = %Q<(?:\\[(?:#{ipv4_address}|#{ipv6_address})\\])>

##################################
# domain

# 一つのサブドメインは63バイト以内 (RFC 1034 section 3.5)
sub_domain = %Q<[A-Za-z0-9]#{ldh_str}?>
toplabel = %Q<[A-Za-z]#{ldh_str}?>

# RFC 2609 の制限 (トップレベルは英字必須)
# domain は 255オクテットまで. (RFC 5321 section 4.5.3)
domain = %Q<(?:(?:#{sub_domain}\\.){0,127}#{toplabel})>

# RFC 5321 による.
mailbox = %Q<(#{local_part})@(#{domain}|#{address_literal})>   # $1, $2

$mail_regex = Regexp.new(mailbox)


# test ###########################3

print "local_part = ", Regexp.new(local_part), "\n"
print "domain = ", Regexp.new(domain), "\n"
print "address_literal = ", Regexp.new(address_literal), "\n"
print "mailbox = ", $mail_regex, "\n"

str = ('"\\' * 1700 + '"@hoge')
puts str
p $mail_regex =~ str #=> nil. 一瞬で完了する. OK
