# -*- coding:utf-8 -*-

# メイルアドレスの正規表現をテストする

require "rubygems"
require "erubis/helper"
include Erubis::XmlHelper # h()

require_relative "make-email-regexp"

print <<EOF
<table style="table-layout:fixed; width:1600px">
  <colgroup>
    <col style="width:400px" />
    <col />
    <col />
    <col />
  </colgroup>
EOF

File.open("test-cases.txt").each_line do |line|
  line = line.strip
  next if line == "" || line[0..0] == "#"
  ary = line.split(/\t/)
  raise line if ary[1] != 'Y' && ary[1] != 'N'

  print "  <tr><td>#{h ary[0]}  "
  if ary[1] == 'Y'
    print "<td>Y  "
    if $mail_regex =~ ary[0] && $& == ary[0]
      print "<td style=\"color:green\">MATCHED  "
    else
      print "<td style=\"color:red; font-weight:bold\">#{h $&}, $1 = #{h $1}, $2 = #{h $2}  "
    end
  else
    print "<td>N  "
    if $mail_regex =~ ary[0] && $& == ary[0]
      print "<td style=\"color:red; font-weight:bold\">MATCHED  "
    else
      print "<td style=\"color:green\">#{h $&}, $1 = #{h $1}, $2 = #{h $2}  "
    end
  end
  print "<td>#{h(ary[2])}\n"

end # each_line
