#!/usr/local/bin/ruby

#LOC = "/usr/local/lib/ruby/site_ruby/#{RUBY_VERSION.split('.')[0..1].join('.')}.0"
LOC = "/opt/rbenv/versions/#{RUBY_VERSION}/lib/ruby/site_ruby/#{RUBY_VERSION.split('.')[0..1].join('.')}.0"

FILES = {"lib/common.rb" => "common.rb",
         "lib/csconv.rb" => "csconv.rb",
         "wafu2" => "wafu2", 
         "rails_sup" => "rails_sup"}


FILES.each do |file, tofile|
  raise if !File.exist?(file)

  newfile = LOC + "/" + tofile

  File.unlink newfile if File.exist?(newfile)
  File.symlink ENV["PWD"] + "/" + file, newfile
end
