


## <code>MailAddrValidator &lt; ActiveModel::EachValidator</code>

メイルアドレスの妥当性確認. RFC 5321 (RFC 5322 ではない.) の定義から直接、導出した。次のような、変わったものも正しく妥当性検査できる。

妥当
 - <code>"ho@ge"@example.com</code>
 - <code>email@[123.123.123.123]</code>
 - <code>firstname+lastname@domain.com</code>
 - <code>"===test..123"@example.jp</code>
 - <code>#%&'/=~`*+?{}^$-|.def@ghi.com</code>
 
非妥当
 - <code>email@123.123.123.123</code>
 - <code>.email@domain.com</code>
 - <code>firstname+last..name@domain.com</code>

正規表現で、一撃で判定する。

```pcre
^(?-mix:((?:[A-Za-z0-9!#$%&'*+\-\/=?^_`{|}~]{1,64}(?:\.[A-Za-z0-9!#$%&'*+\-\/=?^_`{|}~]{1,62}){0,31}|"(?:[\x21\x23-\x5B\x5D-\x7E]|\\[\x20-~]){0,62}"))@((?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])?\.){0,127}[A-Za-z](?:[A-Za-z0-9-]{0,61}[A-Za-z0-9])?)|(?:\[(?:(?:[0-9]{1,3}(?:\.[0-9]{1,3}){3})|(?:(?:[0-9A-Fa-f]{1,4})?(?:(?:::|:)(?:[0-9A-Fa-f]{1,4})){1,7}(?:(?:::|:)(?:[0-9]{1,3}(?:\.[0-9]{1,3}){3}))?))\])))$
```

実装上の注意: 単に `+` や `*` を使うと、いくらでも長いテキストにマッチしてしまう。`{m,n}` で長さを制約すること。ReDoS 対策を兼ねる。

