# -*- coding: utf-8 -*-


# 順序付きのset
# 非常に手抜き。TODO: 大きなデータを扱うときは作り直す
class OrderedSet
  include Enumerable

  # @param [Proc] less_fn   2引数の比較関数
  def initialize less_fn = nil
    raise TypeError if less_fn && !less_fn.is_a?(Proc)

    @compare = less_fn || (lambda do |x, y| x < y end)
    @entries = []
  end

  # @param entry  追加するオブジェクト
  # @return self
  def << entry
    @entries.each_with_index do |e, i|
      next if @compare.call(e, entry)

      if !@compare.call(entry, e)
        # raise @entries.inspect + " << " + entry.inspect # DEBUG
        @entries[i] = entry
        return self
      else
        @entries.insert i, entry
        return self
      end
    end

    @entries << entry
    return self
  end

  def [] index
    raise TypeError if !index.is_a?(Integer)

    @entries[index]
  end

  def each &block
    @entries.each &block
    return self
  end

  def empty?
    @entries.empty?
  end

  def include? entry
    index(entry) ? true : false
  end

  # @return 見つからなかったとき nil
  def index entry
    @entries.each_with_index do |e, i|
      next if @compare.call(e, entry)
      return !@compare.call(entry, e) ? i : nil
    end
    nil
  end

  def length
    @entries.length
  end

  # 重複する場合はotherの値を使う
  # @return self
  def merge other
    other.each do |entry| self << entry end
    self
  end

  def dup
    n = super
    n.instance_variable_set :@entries, @entries.dup
    return n
  end
end


if __FILE__ == $0
s = OrderedSet.new
s << 1 << 2 << 4 
p s
s << 3
p s

s1 = OrderedSet.new
s1 << 1
s2 = s1.dup
s2 << 2
p s1, s2
end
