# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/


module Wafu

  # フォームコントロールを生成、など
  module TagHelper

    def tag_ name, attrs = {}
      s = "<#{name}"
      if !attrs.empty?
        # 常にエスケープする
        s << (attrs.collect do |k, v|
                v ? " #{k}=\"#{CGI.escapeHTML(v)}\"" : nil
              end.join )
      end
      s
    end
    private :tag_

    def start_tag name, attrs = {}
      tag_(name, attrs) + ">"
    end

    def empty_tag name, attrs = {}
      tag_(name, attrs) + " />"
    end

    def end_tag name
      "</#{name}>"
    end

  end # TagHelper


  # 次のバージョン (Wafu 2.0) では、form / form_for ブロックの中だけで使えるようにする。
  module FormHelper
    include TagHelper

    def hidden_field(object_name, method_name, options = {})
      make_input_tag "hidden", object_name, method_name, options
    end

    def text_field(object_name, method_name, options = {})
      make_input_tag "text", object_name, method_name, options
    end

    def error_messages_for object_name
      s = ""
      o = instance_variable_get("@#{object_name}")
      raise ArgumentError, "missing object: '#{object_name}'" if !o
      if !o.errors.empty?
        s = "<div class=\"errorExplanation\"><ul>"
        s << o.errors.full_messages.map {|msg| "<li>" + h(msg)}.join
        s << "</ul></div>"
      end
      return s
    end

  private
    def make_input_tag type, object_name, method_name, options
      h = {
        :type => type,
        :name => "#{object_name}[#{method_name}]"
      }
      obj = instance_variable_get("@#{object_name}")
      v = obj.send method_name
      h[:value] = v.to_s if v

      h = h.merge(options) if options

      s = "<input"
      h.each {|k, v|
        s << " #{k}=\"#{v}\""
      }
      s << ">"
      return s
    end

    # ハッシュをクエリ文字列にする.
    # 先頭の'?'は付けない
    def query_str_from_hash params
      p_ary = []
      params.each {|k, v|
        p_ary << [k, CGI.escape(v.to_s)].join("=")
      }
      return p_ary.join("&")
    end
  end

end # of module Wafu
