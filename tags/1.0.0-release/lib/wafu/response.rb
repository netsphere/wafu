# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/


module Wafu

  # HTTPレスポンス
  class Response
    # RFC 2616 HTTP/1.1
    HTTP_STATUS = { 
      100 => 'Continue',
      101 => 'Switching Protocols',
      200 => 'OK',
      201 => 'Created',
      202 => 'Accepted',
      203 => 'Non-Authoritative Information',
      204 => 'No Content',
      205 => 'Reset Content',
      206 => 'Partial Content',
      300 => 'Multiple Choices',
      301 => 'Moved Permanently',
      302 => 'Found',
      303 => 'See Other',
      304 => 'Not Modified',
      305 => 'Use Proxy',
      307 => 'Temporary Redirect',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      406 => 'Not Acceptable',
      407 => 'Proxy Authentication Required',
      408 => 'Request Timeout',
      409 => 'Conflict',
      410 => 'Gone',
      411 => 'Length Required',
      412 => 'Precondition Failed',
      413 => 'Request Entity Too Large',
      414 => 'Request-URI Too Long',
      415 => 'Unsupported Media Type',
      416 => 'Requested Range Not Satisfiable',
      417 => 'Expectation Failed',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      502 => 'Bad Gateway',
      503 => 'Service Unavailable',
      504 => 'Gateway Timeout',
      505 => 'HTTP Version Not Supported'
    }

    def initialize(hash = {})
      @header = hash
    end
    attr_reader :header
    attr_accessor :body
    attr_accessor :status_code
  
    def get_http_header(cgi)
      if status_code
        if HTTP_STATUS[status_code]
          header["status"] = "#{status_code} #{HTTP_STATUS[status_code]}"
        else
          raise "unknown status code"
        end
      end
      return cgi.header(header)
    end

    def redirect?
      case status_code
      when 301, 302, 303, 307
        return true
      else
        return false
      end
    end
  end # of class Response

end # of module Wafu
