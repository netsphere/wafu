# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/


module Wafu

  # フォームデータを検査する
  class ActionForm
    def initialize app
      @app = app
      @errors = []
      setup()
    end
    attr_reader :app, :errors

    def validate
      return set_error(nil)
    end

    def setup; end

    def error?
      return errors && errors.size > 0
    end

    def set_error(error_ary)
      if error_ary
        raise TypeError if !error_ary.is_a?(Array)
        @errors += error_ary
      end
      return self
    end

    # フォームデータと同名のプロパティを設定する。
    # @param hash フォームデータの名前と型. {名前 => 型}
    # @deprecated  新しいプログラムでは [] を使うこと
    def set_cgi_attrs hash
      @attr_keys = []
      hash.each {|name, type|
        v = app.cgi.get(name)
        instance_variable_set "@#{name}", (case type
          when "bool"
            v && v != "" && v != "0" ? true : false
          when Integer, "int"
            v.to_i
          else
            v
          end
        )
        ActionForm.module_eval "def #{name}; @#{name} end"
        @attr_keys << name.to_s
      }
    end

    # @param key  "[...][...]" の形
    def create_params_sub(ptr, key, data)
      j = key.index(']', 1)
      raise ArgumentError, "format error: key = '#{key}'" if key[0,1] != '[' || !j
      raise ArgumentError, "key missing" if j < 2

      if key.length <= j + 1
        ptr[key[1..j-1]] = data
      else
        create_params_sub(ptr[key[1..j-1]] ||= {}, key[j+1..-1], data)
      end
    end
    private :create_params_sub

    # フォームデータからハッシュを構築する
    def create_params
      @params = {}
      cgi_params = app.cgi.params

      # Ruby 1.8.7のcgi.rbは、postのとき、クエリ文字列を読まない
      if ENV["REQUEST_METHOD"] == "POST" && 
                        (ENV["QUERY_STRING"] && ENV["QUERY_STRING"] != "")
        cgi_params.update CGI.parse(ENV["QUERY_STRING"])
      end

      cgi_params.each {|k, v|
        if k[-2..-1] == "[]"
          data = v
          k = k[0..-3]
        else
          data = v[0]
        end

        if !(i = k.index('['))
          @params[k] = data
        else
          raise ArgumentError, "key missing" if i == 0
          create_params_sub(@params[k[0..i-1]] ||= {}, k[i..-1], data)
        end
      }
    end
    private :create_params

    # ハッシュの階層を返す
    # input name="partner[name]" => key="partner"のハッシュ
    # input name="data[]" => key="data"の配列
    def [](key)
      create_params if !@params
      return @params[key] 
    end

    # ActiveRecord対策
    # attributes=() にActionFormインスタンスを渡せるようにする。
    def stringify_keys!
      # empty
    end

    def each
      create_params if !@params
      @params.each {|k, v|
        yield k, v
      }
    end
    
    def reject
      create_params if !@params
      result = {}
      @params.each {|k, v|
        result[k] = v if !(yield k, v)
      }
      return result
    end
  end # of class ActionForm

end # of module Wafu
