
require "cgi"

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end
require File.rel2abs("./strstore.rb", __FILE__)

class DBProfile
  attr_reader :driver_uri, :user, :passwd

  def initialize(driver_uri, user, passwd)
    @driver_uri = driver_uri
    @user = user
    @passwd = passwd
  end

  def self.parse(s)
    raise TypeError if !s.is_a?(String)
    a = s.split("\t")
    a.map! {|e| CGI.unescape(e)}
    a[1] = nil if a[1] == ""
    a[2] = nil if a[2] == ""
    raise ArgumentError if a[0] =~ /[^a-zA-Z0-9\:]/
    a[0].untaint
    return DBProfile.new(a[0], a[1], a[2])
  end

  def to_s
    [CGI.escape(@driver_uri), CGI.escape(@user), CGI.escape(@passwd)].join("\t")
  end
end

class DBPasswd
  def initialize(filename)
    raise TypeError if !filename.is_a?(String)
    raise SecurityError, "file name is tainted." if filename.tainted?

    @passwd_file = filename
  end

  def get_profile(dbname)
    check_dbprofile(dbname)
    StrStore.new(@passwd_file).transaction_ro {|profdb|
      if !profdb.key?(dbname)
        raise ArgumentError, "not found the profile '#{dbname}' on '#{@passwd_file}'"
      end
      return DBProfile.parse(profdb[dbname])
    }
  end

  def set_profile(dbname, profile)
    raise TypeError if !profile.is_a?(DBProfile)
    check_dbprofile(dbname)
    StrStore.new(@passwd_file).transaction {|profdb|
      profdb[dbname] = profile.to_s
    }
  end

  private
  def check_dbprofile(dbname)
    raise TypeError if !dbname.is_a?(String)
    raise SecurityError, "database name is tainted." if dbname.tainted?

    expath = File.dirname(File.expand_path(@passwd_file)).untaint
    path = ""
    expath.split("/").each {|p|
      path += p + "/"
      st = File.stat(path)
      raise SecurityError, "'#{path}' is world-writable" if (st.mode & 0022) != 0
    }

    if FileTest.exist?(@passwd_file)
      st = File.stat(@passwd_file)
      raise SecurityError, "'#{@passwd_file}' must have mode 0600" if (st.mode & 0177) != 0
    else
      File.open(@passwd_file, "a") {|fp| fp.chmod(0600)}
    end
  end
end

# for backward compatibility 
def get_dbprofile(filename, dbname)
  DBPasswd.new(filename).get_profile(dbname)
end
