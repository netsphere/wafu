
# -*- mode:ruby; encoding:euc-jp -*-

# ruby library
# Copyright (c) 1999-2003 HORIKAWA Hisashi. All rights reserved.
#     mailto:vzw00011@nifty.ne.jp
#     http://www.nslabs.jp/

# 細々とした関数群

require "socket"

# ファイル名の拡張子を得る
# 拡張子には、ピリオドは含まない
def get_ext_name(pathname, ext = nil)
  base = File.basename(pathname)
  r = base.rindex('.')
  if r == nil || r == 0
    return ext
  else
    return base[r + 1, base.length]
  end
end

# 3桁ごとに','を入れる
def decimal_format(s)
  return nil if !s
  if s >= 0
    str = s.to_s.reverse
    return str.scan(/.?.?./).join(',').reverse
  else
    str = (-s).to_s.reverse
    return "-" + str.scan(/.?.?./).join(',').reverse
  end
end

def get_counter(fname)
  File.open(fname, "r") {|count_f|
    count_f.flock(File::LOCK_SH)  
    return count_f.gets.to_i
  }
end

# カウンタを加算し，（加算後の）新しい番号を返す
def incr_counter(fname, mode = File::RDWR | File::CREAT)
  cnt = nil
  File.open(fname, mode) {|fp|
    fp.flock(File::LOCK_EX)
    cnt = fp.gets.to_i + 1
    fp.rewind
    fp.truncate(0)
    fp.puts(cnt)
  }
  return cnt
end

# XMLのタグの属性を取り出す
# s = "attr="val" attr="val">" # 要素名は含まない
def get_attrs(s)
  r = Hash.new
  while s =~ /([a-zA-Z][a-zA-Z0-9]*)[ \t\r\n]*=[ \t\r\n]*\"([^\"]*)\"/
    attr_name = $1.downcase
    attr_value = $2.strip
    s = $'
    r[attr_name] = attr_value
  end
  r
end

# 1. IPアドレス文字列からホスト名を得る
# 2. ホスト名を正規化する
# @return 
#     IPアドレス文字列が与えられ、ホスト名が解決できないときは、元のIPアドレス文字列を返す。
#     ホスト名が与えられ、それが存在しなかったときは、nilを返す。
def get_hostname(ipstr)
  return nil if !ipstr
  raise TypeError if !ipstr.is_a?(String)

  begin
    return Socket.getnameinfo([nil, nil, ipstr])[0]
  rescue SocketError
    begin
      return Socket.gethostbyname(ipstr)[0]
    rescue SocketError
      return nil
    end
  end
end
