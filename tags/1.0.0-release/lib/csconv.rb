# -*- coding:utf-8 -*-

# 文字列の変換。nkf-utf8が使えるときはnkfを使い、
# Rubyのバージョンがそれ以下のときはUConv / IConvを使う。


class CS_UConv
  def self.u2e(s)
    return s ? Uconv.u8toeuc(s) : nil
  end

  def self.u2s(s)
    return s ? Uconv.u8tosjis(s) : nil
  end

  def self.u2j(s)
    return s ? e2j(Uconv.u8toeuc(s)) : nil
  end

  def self.e2u(s)
    return s ? Uconv.euctou8(s) : nil
  end

  def self.s2u(s)
    return s ? Uconv.sjistou8(s) : nil
  end

  def self.j2u(s)
    return s ? Uconv.euctou8(j2e(s)) : nil
  end
end


class CS_NKF
  def self.u2e(s)
    s ? NKF.nkf("-W -e -x -m0", s) : nil
  end

  def self.u2s(s)
    s ? NKF.nkf("-W -s -x -m0", s) : nil
  end

  def self.u2j(s)
    s ? NKF.nkf("-W -j -x -m0", s) : nil
  end

  def self.e2u(s)
    s ? NKF.nkf("-E -w -x -m0", s) : nil
  end

  def self.s2u(s)
    s ? NKF.nkf("-S -w -x -m0", s) : nil
  end

  def self.j2u(s)
    s ? NKF.nkf("-J -w -x -m0", s) : nil
  end
end


class CS_IConv
  def self.u2e(s)
    return s ? Iconv.iconv(SystemConfig[:eucjp_ces], "UTF-8", s).first : nil
  end

  def self.u2s(s)
    return s ? Iconv.iconv(SystemConfig[:sjis_ces], "UTF-8", s).first : nil
  end

  def self.u2j(s)
    return s ? e2j(Iconv.iconv(SystemConfig[:eucjp_ces], "UTF-8", s).first) : nil
  end

  def self.e2u(s)
    return s ? Iconv.iconv("UTF-8", SystemConfig[:eucjp_ces], s).first : nil
  end

  def self.s2u(s)
    return s ? Iconv.iconv("UTF-8", SystemConfig[:sjis_ces], s).first : nil
  end

  def self.j2u(s)
    return s ? Iconv.iconv("UTF-8", SystemConfig[:eucjp_ces], j2e(s)).first : nil
  end
end

class CSConv
  def self.load_converter()
    return if @conv
    if RUBY_RELEASE_DATE >= "2004-10-29" # 1.8.1 -> 1.8.2
      require "nkf"
      @conv = CS_NKF
    else
      if SystemConfig[:use_uconv]
        require "uconv"
        @conv = CS_UConv
      else
        require "iconv"
        @conv = CS_IConv
      end
    end
  end

  def self.u2e(s)
    load_converter()
    @conv.u2e(s)
  end

  def self.u2s(s)
    load_converter()
    @conv.u2s(s)
  end

  def self.u2j(s)
    load_converter()
    @conv.u2j(s)
  end

  def self.e2u(s)
    load_converter()
    @conv.e2u(s)
  end

  def self.s2u(s)
    load_converter()
    @conv.s2u(s)
  end

  def self.j2u(s)
    load_converter()
    @conv.j2u(s)
  end
end

# for backward compatibility

def u2j(s) # UTF-8 -> iso-2022-jp
  CSConv.u2j(s)
end

def j2u(s)
  CSConv.j2u(s)
end

def u2e(s)
  CSConv.u2e(s)
end

def e2u(s)
  CSConv.e2u(s)
end

=begin
def e2s(s) # euc-jp -> shift_jis
  s ? NKF.nkf("-s -E -x -m0", s) : nil
end

def s2e(s)
  s ? NKF.nkf("-S -e -x -m0", s) : nil
end
=end
