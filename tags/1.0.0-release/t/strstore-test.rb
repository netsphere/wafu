
require "runit/testcase"
require "runit/cui/testrunner"

require "../strstore.rb"

class StrStoreTest < RUNIT::TestCase
  def setup
  end

  def teardown
    if FileTest.directory?("test")
      File.unlink("test/testfile") if FileTest.exist?("test/testfile")
      Dir.rmdir("test")
    end
  end

  def test11
    assert_exception(ArgumentError) {
      StrStore.new("./hogehoge/hogehoge")
    }
  end

  def test12
    Dir.mkdir("test")
    File.open("test/testfile", "w") {|fp| fp.chmod(000)}
    assert_exception(ArgumentError) {
      StrStore.new("test/testfile")
    }
  end

  def test13
    Dir.mkdir("test")
    File.chmod(000, "test")
    assert_exception(ArgumentError) {
      StrStore.new("test/testfile")
    }
  end

  # []=
  def test21
    Dir.mkdir("test")
    db = StrStore.new("test/testfile")
    assert_exception(RuntimeError) {
      db["hoge"] = "huga"
    }
    db.transaction_ro {
      assert_exception(RuntimeError) {
        db['foo'] = "FOO"
      }
    }
    db.transaction {
      assert_exception(TypeError) {
        db[1] = "12"
      }
      s = "12".taint
      assert_exception(SecurityError) {
        db[s] = "taint"
      }
      assert_exception(ArgumentError) {
        db["hoge=hoge"] = "foo"
      }
      db["foo"] = "bar"
    }
    db.transaction_ro {
      assert_equal("bar", db["foo"])
    }
  end

  def test31
    Dir.mkdir("test")
    db = StrStore.new("test/testfile")
    assert_exception(RuntimeError) {
      p db["foo"]
    }
    db.transaction {
      db["foo"] = "llllllllllllllllllonggggggggggggg"
      db["foo"] = "s"
      assert_equal("s", db['foo'])
      assert_exception(IndexError) {
        p db["none"]
      }
    }
  end

  def test41
    Dir.mkdir("test")
    db = StrStore.new("test/testfile")
    db.transaction {
      db["foo"] = "bar"
      db.commit
      db["foo"] = "baz"
    }
    db.transaction_ro {
      assert_equal("bar", db["foo"])
    }
    db.transaction {
      db["foo"] = "test"
      db.abort
    }
    db.transaction_ro {
      assert_equal("bar", db["foo"])
    }
  end

  def test51
    Dir.mkdir("test")
    db = StrStore.new("test/testfile")
    db.transaction {
      db["foo"] = "bar"
      db.delete("foo")
      assert_equal(false, db.key?("foo"))
    }
    db.transaction_ro {
      assert_equal(false, db.key?("foo"))
    }
  end
end

RUNIT::CUI::TestRunner.run(StrStoreTest.suite)
