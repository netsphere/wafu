
require "runit/testcase"
require "runit/cui/testrunner"

require "./cgisup.rb"

class CGISupTest < RUNIT::TestCase
  def test1_get_time
    expected = Time.gm(1994, 11, 6, 8, 49, 37)
    assert_equal(expected, ParseDate.get_time("Sun, 06 Nov 1994 08:49:37 GMT"))
    assert_equal(expected, ParseDate.get_time("Sunday, 06-Nov-94 08:49:37 GMT"))
    assert_equal(expected, ParseDate.get_time("Sun Nov  6 08:49:37 1994"))
  end

  def setup
    File.unlink "./session/test1234" if FileTest.exist? "./session/test1234"
  end

  def test2_session_exist
    option = {"tmpdir" => "./session", "prefix" => "test"}
    cgi = {"_session_id" => "1234"}
    assert_equal(false, CGI::Session.exist?(cgi, option))
    session = CGI::Session.new(cgi, option)
    assert_equal(true, CGI::Session.exist?(cgi, option))
  end

  def test3_sweep
    option = {"tmpdir" => "./session", "prefix" => "test",
              "holdtime" => 5 * 60}
    cgi = {"_session_id" => "1234"}
    session = CGI::Session.new(cgi, option)
    session['_last-accessed'] = CGI.rfc1123_date(Time.now)
    CGI::Session.sweep(option)
    assert_equal(true, CGI::Session.exist?(cgi, option))
    session['_last-accessed'] = CGI.rfc1123_date(Time.now - 6 * 60)
    session.update
    CGI::Session.sweep(option)
    assert_equal(false, CGI::Session.exist?(cgi, option))
  end
end

RUNIT::CUI::TestRunner.run(CGISupTest.suite)
