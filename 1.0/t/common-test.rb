
require "runit/testcase"
require "runit/cui/testrunner"

require "./common.rb"
require "parsedate"
require "cgi"
require "date"

class GetTimeTest < RUNIT::TestCase
  def test1
    expected = Time.gm(1994, 11, 6, 8, 49, 37)
    assert_equal(expected, get_time("Sun, 06 Nov 1994 08:49:37 GMT"))
    assert_equal(expected, get_time("Sunday, 06-Nov-94 08:49:37 GMT"))
    assert_equal(expected, get_time("Sun Nov  6 08:49:37 1994"))
  end

  def test2
    assert_equal(nil, get_time(nil))
    assert_equal(nil, get_time(""))
  end

  def to_time(a)
    Time.gm *a[0..-3]
  end
  
  def test_parsedate
    expected = Time.gm(1994, 11, 6, 8, 49, 37)
    assert_equal(expected,
                 to_time(ParseDate.parsedate("Sun, 06 Nov 1994 08:49:37 GMT")))
    assert_equal(expected,
                 to_time(ParseDate.parsedate("Sunday, 06-Nov-94 08:49:37 GMT")))
    assert_equal(expected,
                 to_time(ParseDate.parsedate("Sun Nov  6 08:49:37 1994")))
  end
end

RUNIT::CUI::TestRunner.run(GetTimeTest.suite)
