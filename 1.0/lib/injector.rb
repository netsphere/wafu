# -*- mode:ruby; coding:utf-8 -*-

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end

require 'nkf'
if RUBY_VERSION >= "2.0"
  # mailreadは廃止された
  gem "mail"
  require "mail"
else
  require 'mailread'  # class Mail
end
require_relative "csconv"

if __FILE__ != $0
  if !defined?(SystemConfig) || !SystemConfig[:mail_injector]
    raise "need set SystemConfig[:mail_injector]" 
  end
end


if RUBY_VERSION < "2.0"
# 空インスタンスを生成できるようにし、メソッドを追加する。
class Mail
  # 'mail' では, Mail.new は Mail::Message インスタンスを返す
  alias :orig_initialize :initialize

  def initialize(io = nil)
    if io
      orig_initialize(io)
    else
      @header = {}
      @body = []
    end
  end


  def header= hash
    raise TypeError if !hash.is_a?(Hash)

    @header.clear
    hash.each do |k, v|
      @header[k.capitalize] = v   # see mailread.rb
    end
  end


  def body=(ary)
    if ary.is_a?(Array)
      @body = ary
    elsif ary.is_a?(String)
      @body = ary.split(/\r?\n|\r/)
    else
      raise TypeError
    end
  end


  def charset
    # see RFC 2045
    return nil if !self["content-type"] 
    t = self["content-type"].split(/\;[ ]*/, 2)
    return nil if t.size < 2
    c = t[1].split(/\=/, 2)
    if c[0].downcase == "charset"
      return c[1]
    else
      return nil
    end
  end
end
end # RUBY_VERSION < "2.0"


class String
  # 文字列（UTF-8と仮定）から1文字取り出す。
  def shift_char()
    return nil if self == ""
    a = split(//u, 2)
    replace(a[1])
    a[0]
  end

  def unshift(s)
    self[0, 0] = s
  end
end


# メールアドレスの妥当性を（簡便に）確認する。
# @return 妥当ならtrue
def valid_mail_addr(mail)
  return false if !mail.is_a?(String)
  return false if mail =~ /[^a-zA-Z0-9\.\-\+@_]/ || mail !~ /@/
  return true
end


# メールヘッダーを生成する
# @param [String] name ヘッダーフィールド名
# @param [String] value ヘッダーフィールド値 (UTF-8を仮定)
def mail_header(name, value)
  raise TypeError if !name.is_a?(String)
  raise TypeError, "key '#{name}''s value expect aString, but #{value.class}" if !value.is_a?(String)
  raise ArgumentError, "value includes control code" if value =~ /[\x00-\x1f\x7f]/
 
  if value !~ /[^\x20-\x7e]/
    # nkfは、ASCIIだけのときはエンコードしない。
    return name + ": " + value + "\n"
  else
    # (2007.7.18) nkfは行の長さが折り返し長と同じときだけ、"\n "を末尾に付けてしまう。
    return name + ": " + NKF.nkf('-W -j -m0 -M', value).strip.chomp + "\n"
  end
end


# メールを送信する
# @param header ヘッダ. 文字コードはUTF-8を仮定
# @param body   本文。文字コードはUTF-8を仮定
def send_mail header, body
  raise TypeError, "header must be a Hash" if !header.is_a?(Hash)
  raise TypeError, "body must be a String or Array" if !body.is_a?(String) && !body.is_a?(Array)

  begin
    pipe = IO.popen("#{SystemConfig[:mail_injector]}", "w")
    header.each {|k, v|
      pipe.print mail_header(k, v)
    }
    pipe.print "\n"
    body_ = body.is_a?(Array) ? body : body.split(/\r?\n|\r/)
    body_.each {|line|
      pipe.print NKF.nkf("-j -W -m0 -x -f70", line.chomp + 'a').chomp.chop, "\n"
    }
  ensure
    pipe.close if pipe
  end
end
