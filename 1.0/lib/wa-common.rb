
# -*- mode:ruby; encoding:euc-jp -*-

# Copyright (c) 2003 HORIKAWA Hisashi. All rights reserved.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, and/or sell copies of the
# Software, and to permit persons to whom the Software is furnished to do so,
# provided that the above copyright notice(s) and this permission notice appear
# in all copies of the Software and that both the above copyright notice(s) and
# this permission notice appear in supporting documentation.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
# NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE
# LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
# DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Except as contained in this notice, the name of a copyright holder shall not
# be used in advertising or otherwise to promote the sale, use or other dealings
# in this Software without prior written authorization of the copyright holder.

class File
  def File.rel2abs(rel_path, this_file)
    File.expand_path(rel_path, File.dirname(File.expand_path(this_file)))
  end
end

require File.rel2abs("./webapp.rb", __FILE__)
require File.rel2abs("./common.rb", __FILE__)
require "amrita/template"

def get_hash(s)
  s ? Digest::MD5.new(s).hexdigest : nil
end

def create_random_id()
  return get_hash(Time.now.to_f.to_s + rand().to_s)
end

# 月、日からDateオブジェクトを生成する
def get_short_dated(mon, day)
  raise TypeError if !mon.is_a?(Integer)
  raise TypeError if !day.is_a?(Integer)

  today = Date.today
  if (jd = Date.exist?(today.year, mon, day)) && Date.new1(jd) >= today - 10
    return Date.new1(jd)
  elsif jd = Date.exist?(today.year + 1, mon, day)
    return Date.new1(jd)
  else
    return nil
  end
end

def get_rubydate(obj)
  if !obj
    return nil
  elsif obj.is_a?(Date)
    return obj
  elsif obj.is_a?(DBI::Date)
    return Date.new(obj.year, obj.mon, obj.day)
  else
    raise TypeError, "in = #{obj}"
  end
end

DAYNAMES_JA = %w(日 月 火 水 木 金 土)
def format_date(date)
  raise TypeError if !date.is_a?(Date)
  sprintf("%04d.%02d.%02d(%s)", date.year, date.mon, date.day,
          DAYNAMES_JA[date.wday])
end

class Base < Wafu::WebApp
  attr_accessor :session
  def initialize(auth_type = nil)
    opt = {
      "session_type" => "auth_required",
      "session_option" => SESSION_OPTION
    }
    super(opt)

    @auth_type = auth_type   # "index", or nil
    @db = DBAccessor.new
  end

  def check_passwd(userid_, pwd_)
    raise TypeError if !userid_.is_a?(String)
    raise TypeError if !pwd_.is_a?(String)

    if @db.check_passwd(userid_, get_hash(pwd_))
      return Wafu::LOGON_SUCCESS
    else
      return Wafu::LOGON_RETRY
    end
  end

  def login_form(state)
    if @auth_type == "index"
      logon(state == Wafu::LOGON_RETRY)
    else
      to_logon()
    end
  end

  # for backward compatibility
  alias :set_transaction_data :set_viewstate_data
  alias :get_transaction_data :get_viewstate_data
end

# 第21章、第23章で共通のコントローラ選択モジュール
# for backward compatibility 
module AuthedApp
  def self.start(controller_class)
    controller_class.new.run()
  end
end
