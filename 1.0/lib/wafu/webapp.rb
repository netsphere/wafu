# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# このライブラリを使っているプロジェクト：
#    Q's Web Account Book
#    Q's WebDiary2
#    common/wa-common.rb
#    Q's FotoAlbum


#LIBRARY_DIR = ".." # DEBUG

require "cgisup"
require "injector"  # send_mail()

require "wafu/action_form"
require "wafu/action"
require "wafu/response"


class NilClass
  # Raises a RuntimeError when you attempt to call +id+ on +nil+.
  # nil.id => 4 なので、うっかり呼び出すと誤動作の元。
  # active_support/whiny_nil から
  def id
    raise RuntimeError, "Called id for nil -- if you really wanted the id of nil, use object_id", caller
  end
end


# Web Application Framework 'U'
module Wafu  

  LOGON_SUCCESS = 0
  LOGON_RETRY = 1
  LOGON_EXPIRE = 2

  class WebApp
    attr_reader :cgi, :session, :user
    attr_reader :response

    # アプリケーションオブジェクトを生成する。
    # @param [Hash] opt_hash
    #   "session_type"
    #          "none"             no session
    #          "auto"             create session automaticly
    #          "auth_required"    require user auth
    #   "session_option"  CGI::Session's option
    # セッション種別が"auth_required"のときは、Apacheで認証するか、自分で認証するか。
    # 自分で認証するときは、次の値も必要。
    #   "pwdfile"         password file name.
    #   "passwd_store"    a object which has check_passwd().
    def initialize(opt_hash)
      @cgi = CGI.new

      # fix a bug.
      if RUBY_VERSION == '1.8.1'
        class << @cgi
          undef_method :fieldset
        end
      end

      @wa_options = opt_hash
      @response = Wafu::Response.new(opt_hash["response_option"] || {})
    end

    # userid, pwdが正しいか検査する。
    # overrideして、別のフォーム値（ダイジェストなど）を使ってもよい。
    # @return
    #    ユーザーが存在し、かつパスワードが正しい場合 => LOGON_SUCCESS
    #    ユーザーIDまたはパスワードが違う場合 => LOGON_RETRY
    def check_passwd()
      userid_ = cgi.get("user", nil)
      pwd_ = cgi.get("pwd", nil)
      return [LOGON_RETRY, nil] if !userid_ || userid_ == "" || !pwd_ || pwd_ == ""

      if @wa_options['passwd_store']
        store = @wa_options['passwd_store']
      elsif defined?(PasswdStore)
        store = PasswdStore.new(@wa_options['pwdfile'])
      else
        raise "must set passwd_store or pwdfile option."
      end

      if store.check_passwd(userid_, pwd_)
        return [LOGON_SUCCESS, userid_]
      else
        return [LOGON_RETRY, nil]
      end
    end
    private :check_passwd

    # ユーザをログオフさせる
    def logoff_user
      session.delete
      @user = nil
    end

    # セッション情報を得る
    # @return [cgi, session, userid]
    def get_session()
      session_option = @wa_options["session_option"]

      is_session_exist = CGI::Session.exist?(cgi, session_option)
      CGI::Session.sweep(session_option)

      session = nil
      r_state = nil
      case @wa_options["session_type"]
      when "auth_required"
        if cgi.get('login', nil)
          # ログインした・ログインしなおした
          r_state, userid_ = check_passwd()
          if r_state == LOGON_SUCCESS
            session = CGI::Session.new(cgi, session_option)
            session["user"] = userid_

            session.update_access_time() 
            return [session, userid_, r_state]
          else
            return [nil, nil, r_state]
          end
        end

        session = CGI::Session.get(cgi, session_option)
        if !session || !session["user"] || session["user"] == ""
          if ENV["REMOTE_USER"] 
            # HTTP Basic認証
            session = CGI::Session.new(cgi, session_option) if !session
            session["user"] = ENV["REMOTE_USER"].dup
          else
            if is_session_exist
              r_state = LOGON_EXPIRE
            else
              r_state = nil
            end
            return [nil, nil, r_state]
          end
        end
      when "auto"
        session = CGI::Session.new(cgi, session_option)
      end

      session.update_access_time() 
      return [session, session["user"].untaint, r_state]
    end
    private :get_session

    def setup; end
    def teardown; end

    # ごく小さいアプリケーション用の主関数。
    # サブクラスで必ずオーバーライドすること。
    # @deprecated TODO: このメソッドの廃止
    def main()
      raise "no #{self.class}'s main() method."
    end

    # サブクラスで必ずオーバーライドすること。
    def login_form(state)
      raise "no #{self.class}'s login_form() method."
    end

    # 実行する。
    # new() のオプションで'actions'を与えたときはAction (のサブクラス)
    # のexecuteメソッドを呼び出す。
    # そうでないときはWebAppサブクラスのmain()を呼び出す。
    def run()
      session_type = @wa_options["session_type"]
      if session_type == "auto" || session_type == "auth_required"
        @session, @user, state = get_session()
        if session_type == "auth_required" && (!@user || @user == "")
          # raise "session missing!: session=#{@session.inspect}, user = #{@user.inspect}" # DEBUG
          login_form(state)
          exit
        end
      end

      setup()
      if !(actions = @wa_options['actions'] || @wa_options[:actions])
        main
      else
        dispatch(actions)
      end
      teardown()
      
      if response.redirect?
        print response.get_http_header(cgi)
        print <<EOF
<html>
<body>
<p>See <a href="#{response.header["Location"]}">#{response.header["Location"]}</a>
</body>
</html>
EOF
      else
        if response && response.body && response.body != ""
          print response.get_http_header(cgi)
          print response.body
        end
      end
    rescue StandardError, ScriptError
      send_error_mail($!.dup, $@.dup)
      internal_error_message_out()
    end

    # +actions+に従ってActionサブクラスのexecuteメソッドを呼び出す。
    # アクションはCGIフォームデータの"action.foo"コントロール名か
    # "action"コントロール値によって指定する。
    def dispatch(actions)
      action = nil
      actions.each {|k, pair|
        raise TypeError if !pair.is_a?(Array)
        if cgi.get("action." + k, nil)   # input[type=submit]で、name=action.foo
          action = pair
          break
        elsif cgi.get("action") == k     # hiddenコントロールで、name="action", value="foo"
          action = pair
          break
        end
      }
      action = actions['*'] if !action
      raise ArgumentError, "missed `action'" if !action

      af = action[0].new(self) if action[0]
      if af
        af.validate()
        if !af.error?
          aklass = action[1]
        else
          aklass = action[2]
        end
      else
        aklass = action[1]
      end
      raise ArgumentError if !(aklass <= Wafu::Action)

      while aklass.is_a?(Class) && aklass <= Wafu::Action
        aklass = aklass.new(self).execute(af)
      end
    end

    # HTTPリダイレクトを返す.
    # AppConfig[:site_top_url] を設定しておくこと.
    def set_redirect url_path, params = {}
      raise TypeError if !url_path.is_a?(String)

      if url_path.index("http:") != 0 && url_path.index("https:") != 0
        url_path = AppConfig[:site_top_url] + url_path
      end

      if params && params.length > 0
        url_path += "?" + params.collect do |k, v|
                            raise ArgumentError if k =~ /[^a-zA-Z0-9_]/
                            "#{k}=" + CGI.escape(v.to_s)
                          end.join("&")
      end

      response.status_code = 302
      response.header["Location"] = url_path
    end
    alias :redirect_to :set_redirect

    def create_viewstate_id()
      require "digest/md5"
      Digest::MD5.new.update(Time.now.to_f.to_s + rand().to_s).hexdigest
    end

    def get_viewstate_data(idstr)
      raise TypeError if idstr && !idstr.is_a?(String)

      return nil if !idstr || idstr == ""
      if session["vs." + idstr] && session["vs." + idstr] != ""
        r = Marshal.load(session["vs." + idstr])
        return r
      else
        return nil
      end
    end

    def set_viewstate_data(data)
      idstr = create_viewstate_id()
      if data
        session["vs." + idstr] = Marshal.dump(data)
      else
        session["vs." + idstr] = nil
      end
      return idstr
    end

    # 取り出したら削除する。viewstateと違う。
    def get_flash key
      raise TypeError if !key.is_a?(String)
      raise ArgumentError if key != "notice" && key != "alert"

      r = session[key]
      session[key] = nil
      r = nil if r == ""
      return r
    end

    def set_flash key, value
      raise TypeError if !key.is_a?(String)
      raise ArgumentError if key != "notice" && key != "alert"

      session[key] = value
    end

    def internal_error_message_out()
      print <<EOF
Content-Type:text/html

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
An exception raised.
<p>#{CGI.escapeHTML($!.inspect)}
<p>
EOF
      $@.each {|x| print CGI.escapeHTML(x), "<br>\n"}
      print <<EOF
<hr>
#{CGI.escapeHTML(cgi.params.inspect)}
</body>
</html>
EOF
    end

    private
    def send_error_mail(err, at)
      raise RuntimeError if !AppConfig[:admin_mail]

      site_name = AppConfig[:site_name]
      header = {
        "From" => AppConfig[:prog_mail],
        "To" => AppConfig[:admin_mail],
        "Subject" => "#{site_name}: エラー発生",
        "MIME-Version" => "1.0",
        "Content-Type" => "text/plain; charset=ISO-2022-JP"}
      mailbody = <<EOF
#{site_name}: エラー発生

<date>#{Time.now}</date>

<userid>
#{@user.inspect}
</userid>

<ua>
#{ENV["HTTP_USER_AGENT"]}
</ua>

<message>
#{err.inspect}
</message>

<backtrace>
EOF
      at.each {|x| mailbody += x + "\n"}
      mailbody << "</backtrace>\n"
      mailbody << "<request>\n" << cgi.params.inspect << "</request>\n"

      send_mail(header, mailbody)
    end
  end # class WebApp

end # module Wafu

