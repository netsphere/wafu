# -*- coding:utf-8 -*-

# Web Application Framework 'U'
# Copyright (c) 2002-2005,2009-2010 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/


require "erb"  # render()
require "wafu/form_helper"


module Wafu

  # コントローラ
  # サブクラスを作る前提。
  class Action
    include ERB::Util  # h()
    #include RenderHelper
    include FormHelper

    def initialize(app)
      @app = app
    end

  private
    attr_reader :app

    # subclass must override this method.
    def execute af
      raise "no #{self.class}'s execute() method."
    end

    # Ruby on Railsのように、コントローラからrenderを呼び出せるようにする
    def render page_file, options = {}
      if !options[:layout]
        app.response.body = render_partial page_file, options
      else
        body = render_partial page_file, options
        app.response.body = render_partial("_layouts/" + options[:layout]) do
          body
        end
      end
      nil
    end

    # AppConfig[:template_root] の設定が必要。
    # binding を取る都合上、ここで定義しなければならない。
    def render_partial page_file, options = {}
      raise "AppConfig[:template_root] missing." if !AppConfig[:template_root]
      raise ArgumentError if !page_file.is_a?(String)

      s = File.read(AppConfig[:template_root] + '/' + page_file + '.html.erb')
      erb = ERB.new(s, nil, '>')
      erb.filename = page_file
      return erb.result(binding)
    end

    # <a href=...
    #
    # @param [Hash] options 下記以外、HTML aタグの属性になる。
    # @option options [String] :confirm => "question?"  ユーザに確認を求める。
    # @option options [Symbol] :method => :get or :post
    def link_to text, urlpath, params = {}, options = {}
      raise TypeError if !text.is_a?(String)
      raise TypeError if !urlpath.is_a?(String)
      raise TypeError if !options.is_a?(Hash)

      attrs = options.dup
      method = attrs.delete :method
      confirm = attrs.delete :confirm
      raise ArgumentError if method && (method != :get && method != :post)

      if params && params.length > 0
        url = urlpath + "?" + query_str_from_hash(params)
      else
        url = urlpath
      end

      if method == :post
        # 内部でフォームを作る

        # Actionクラスに依存している
        viewstate_id = app.set_viewstate_data 1
        s1 = <<EOF
var f = document.createElement('form');
f.style.display = 'none';
f.method = 'POST';
f.action = '#{url}';
this.parentNode.appendChild(f);
var s = document.createElement('input');
s.setAttribute('type', 'hidden');
s.setAttribute('name', 'viewstate');
s.setAttribute('value', '#{viewstate_id}');
f.appendChild(s);
f.submit();
EOF
        if confirm
          s1 = "if (confirm('" + confirm + "')) {" + s1 + "}" # エスケープ不要
        end

        s = start_tag "a", {
              :href => "#",
              :onclick => s1.gsub(/[\r\n]/, '') + "return false;"
            }.update(attrs)
        s << h(text) << "</a>"
      else
        # method == :get or nil
        if confirm
          attrs[:onclick] = "return confirm('" + confirm + "');" # エスケープ不要
        end
        s = start_tag "a", {:href => url}.update(attrs)
        s << h(text) << "</a>"
      end

      return s
    end

  end

end # of module Wafu
