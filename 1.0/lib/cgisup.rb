# -*- coding: utf-8 -*-

# Ruby CGI Adjuster Library
# Copyright (c) 2002-2005 HORIKAWA Hisashi. All rights reserved.
# You can redistribute this software and/or modify it under the Ruby's License.
#     http://www.nslabs.jp/


# もはやRuby 1.8.6以前ではテストできない
raise "requires ruby 1.8.7" if RUBY_VERSION < "1.8.7"


require "cgisup/session"
# require "digest/md5"
# require "time"


class CGI
  module QueryExtension

    if $COMPAT_VERSION && $COMPAT_VERSION < '1.8.0'
      # ruby 1.6では配列を返す
      # @override
      def [](key)
        return @params[key]
      end
    elsif RUBY_VERSION >= '1.8.1' && RUBY_VERSION < '1.9.0' # v1.8.0だけ修正しない
      # @override
      # 見つからないときは "" を返す
      def [](key)
        return '' if !@params[key]
        value = @params[key].first
        if @multipart
          if value
            return value
          elsif defined? StringIO
            StringIO.new("")
          else
            Tempfile.new("CGI")
          end
        else
          return value ? value.dup : ''
        end
      end
    end


    # StringIO または Tempfile インスタンスを得る。ファイルアップロード用。
    # 追加。
    def file_control(key)
      io, = @params[key]
      if !io
        return nil
      elsif !defined?(io.read)
        raise RuntimeError, "The enctype attribute must be 'multipart/form-data.'"
      else
        return io
      end
    end


    # multipartの場合でも値を得る. 追加
    # @return control's value or default value.
    def get key, default = ""
      return default if !@params[key]
      value = @params[key].first
      if value && defined?(value.read)
        return value.read
      else
        return value || default
      end
    end
  end
end



# メニューコントロールを生成する
# @param name select要素の名前
# @param options 選択肢のvalueの配列（eachメソッドを持つオブジェクト）
# @param default 初期状態で選択されるvalue
# @param block 選択肢の表示文字列
def menu_control(name, options, default = nil)
  s = "<select name=\"#{name}\">\n"
  options.each {|x|
    selected = default == x ? " selected" : ""
    if block_given?
      s << "  <option#{selected} value=\"#{x}\">#{yield(x)}\n"
    else
      s << "  <option#{selected} value=\"#{x}\">#{x}\n"
    end
  }
  s << "</select>"
  if defined?(Amrita)
    return Amrita::SanitizedString[s]
  else
    return s
  end
end

