# -*- coding: utf-8 -*-

# Ruby CGI Adjuster Library
# Copyright (c) 2002-2005 HORIKAWA Hisashi. All rights reserved.
# You can redistribute this software and/or modify it under the Ruby's License.
#     http://www.nslabs.jp/


require "cgi/session"
require "cgisup/file_store"


class CGI
  # cgi/session.rb
  class Session
    include Enumerable

    # @override
    # 
    # @param [Hash] option 各種オプション
    # @option option [String] "session_key"  
    #     セッションキー。省略すると'_session_id'
    # @option option [String] "session_id"
    #     セッションIDを明示的に指定。セキュリティ的に脆弱になるので注意。
    # @option option new_session  
    #     bool値。セッションIDが得られないときに、新しいセッションを開始するか。
    #
    def initialize request, option = {}
      raise TypeError, "request must be a CGI, but #{request.class}" if !request.is_a?(CGI)

      @new_session = false
      session_key = option['session_key'] || '_session_id'
      session_id = option['session_id']
      if !session_id && option['new_session']
        session_id = create_new_id()
        @new_session = true
      end
      if !session_id
        # ここを修正した。
        # CGI#[]を変更したため、ここを修正する必要あり。
        session_id = Session.get_id_in_request request, option
        # 修正ここまで

        if !session_id
          if !option.fetch('new_session', true)
            raise ArgumentError, 
                          "session_key `%s' should be supplied" % session_key
          end
          session_id = create_new_id()
          @new_session = true
        end
      end

      @session_id = session_id
      dbman = option['database_manager'] || FileStore
      begin
        @dbman = dbman.new(self, option)
      rescue NoSession
        if !option.fetch('new_session', true)
          raise ArgumentError, "invalid session_id `%s'" % session_id
        end
        session_id = @session_id = create_new_id() if !session_id
        @new_session = true
        retry
      end

      request.instance_eval do
	@output_hidden = {session_key => session_id} unless option['no_hidden']
	@output_cookies =  [
          Cookie.new("name" => session_key,
		      "value" => session_id,
		      "expires" => option['session_expires'],
		      "domain" => option['session_domain'],
		      "secure" => option['session_secure'],
		      "path" => if option['session_path'] then
				  option['session_path']
		                elsif ENV["SCRIPT_NAME"] then
				  File.dirname(ENV["SCRIPT_NAME"])
				else
				  ""
				end)
        ] unless option['no_cookies']
      end
      @dbprot = [@dbman]
      ObjectSpace::define_finalizer(self, Session.callback(@dbprot))
    end

    class << self
      # 追加
      def get_id_in_request request, option
        session_key = option['session_key'] || '_session_id'
=begin
リクエストパラメータからセッションIDを得るのは、セキュリティ上不味い。
        if request.key?(session_key)
          session_id = request.get(session_key)
        end
=end
        session_id, = request.cookies[session_key]
        return session_id
      end

      # 追加
      def exist? request, option
        return get_id_in_request(request, option) ? true : false
      end

      # 追加
      # セッションが存在すればSessionインスタンスを、そうでなければnilを返す
      def get request, option
        if id = get_id_in_request(request, option)
          return Session.new(request, option.dup.update({"session_id" => id}))
        else
          return nil
        end
      end

      def sweep option
        dbman = option['database_manager'] || FileStore
        dbman.sweep_sessions(option)
      end
    end # class << self

    def update_access_time()
      self["_last-accessed"] = CGI.rfc1123_date(Time.now)
    end

    def each()
      @data ||= @dbman.restore
      @data.each {|k, v|
        if k != "_last-accessed"
          yield k, v
        end
      }
    end
  end
end

