# -*- coding: utf-8 -*-

# Ruby CGI Adjuster Library
# Copyright (c) 2002-2005 HORIKAWA Hisashi. All rights reserved.
# You can redistribute this software and/or modify it under the Ruby's License.
#     http://www.nslabs.jp/


require "cgi/session"


class CGI
  class Session

    class FileStore

      # @option option [String] "tmpdir" セッション情報ファイルの保存場所。必須
      def initialize session, option = {}
#### ここから変更
        @options = option.dup
	# @options['tmpdir'] ||=  Dir::tmpdir
        raise ArgumentError, "no tmpdir option" if !@options["tmpdir"]
        if !File.directory?(@options["tmpdir"])
          raise ArgumentError, "directory of 'tmpdir' missing" 
        end

	dir = @options['tmpdir']
#### ここまで変更 
	prefix = option['prefix'] || ''
	suffix = option['suffix'] || ''
	id = session.session_id
        require 'digest/md5'
        md5 = Digest::MD5.hexdigest(id)[0,16]
	@path = dir + "/" + prefix + md5 + suffix
	if File::exist? @path
	  @hash = nil
	else
          unless session.new_session
            raise CGI::Session::NoSession, "uninitialized session"
          end
	  @hash = {}
	end
      end


      # セッション情報を読み込む。
      # 値として文字列以外も保存できるように修正する
      # @override
      #
      def restore
        @hash = {}
        begin
          File.open @path, "r" do |fp|
            # ロック不要
=begin
            for line in fp
              line.chomp!
              k, v = line.split("=", 2)
              @hash[CGI.unescape(k)] = CGI.unescape(v)
            end
=end
            @hash = Marshal.load fp
          end
        rescue Errno::ENOENT
          # 並行プロセスに削除された
          @hash = {}
        end
        return @hash
      end

      # セッション情報を保存する
      # 値として文字列以外も保存できるようにする。
      # 頭のおかしいロックの仕方を排除
      # @override
      def update
        # ユニークな一時ファイルがキモ
        tmp_fname = @options["tmpdir"] + "/" + make_tmpname
        File.open tmp_fname, "w" do |fp|
=begin
          for k, v in @hash
            f.print CGI.escape(k), "=", CGI.escape(v), "\n"
          end
=end
          Marshal.dump @hash, fp
        end
        File.rename tmp_fname, @path
        return self
      end


      private 

      # ユニークなファイル名を作る
      def make_tmpname
        md5 = Digest::MD5.new
        now = Time.now
        md5.update(now.to_s)
        md5.update(String(now.usec))
        md5.update(String(rand(0)))
        md5.update(String(Process.pid))
        return md5.hexdigest + ".tmp"
      end


      class << self
        # セッション保存ファイルのうち，古いものを削除する
        # @param [Hash] option オプションを格納したハッシュ。
        # @option option [String] "tmpdir" セッション情報ファイルの保存場所。必須
        # @option option [Integer] "holdtime" セッション情報の保持時間（秒数）。必須
        #
        def sweep_sessions option
          dir = option['tmpdir']
          prefix = option['prefix'] || ''
          suffix = option["suffix"] || ""
          holdtime = option['holdtime']
          raise ArgumentError, "no tmpdir option" if !dir || dir == ""
          raise ArgumentError, "tmpdir '%s' missing." % dir if !FileTest.directory?(dir)
          raise ArgumentError, "no holdtime option" if !holdtime

          now = Time.now
          Dir.glob(dir + "/" + prefix + "????????????????" + suffix) do |fname|
            fname.untaint
            begin
              File.open fname, "r" do |fp|
                # ロックしない
=begin
                fp.each_line {|line|
                  k, v = line.chomp.split('=', 2)
=end
                hash = Marshal.load fp
                if (v = hash["_last-accessed"]) && 
                                          Time.rfc2822(v) + holdtime < now
                  File.unlink fp.path.untaint
                end
              end # File.open
            rescue Errno::ENOENT
              # 並行プロセスが削除した。
            end
          end # glob()
        end


        # @param option オプションを格納したハッシュ。tmpdir必須。
        # @return セッションファイルが存在したらtrue
        def exist? session_id, option
          raise TypeError if !session_id.is_a?(String)
          if /[^0-9a-zA-Z]/ =~ session_id
            raise SecurityError, "session_id '#{session_id}' is invalid" 
          end
          dir = option['tmpdir']
          raise ArgumentError, "no tmpdir option" if !dir || dir == ""
          raise ArgumentError, "tmpdir '%s' missing." % dir if !FileTest.directory?(dir)

          # FileStore#initialize()に合わせること。
          if RUBY_RELEASE_DATE >= "2004-12-22"  # v1.8.1 -> 1.8.2
            path = dir + "/" + (option['prefix'] || '') +
                   Digest::MD5.hexdigest(session_id)[0, 16] + 
                   (option['suffix'] || '')
          elsif RUBY_RELEASE_DATE >= "2004-11-16"  # v1.8.1 -> 1.8.2
            path = dir + "/" + (option['prefix'] || 'cgi_sid_') +
                   Digest::MD5.hexdigest(session_id)[0, 16] + 
                   (option['suffix'] || '')
          else
            path = dir + "/" + (option['prefix'] || '') + session_id.dup.untaint
          end
          return FileTest.exist?(path)
        end
      end # class << self
    end # of Session::FileStore

  end # class Session
end # class CGI
