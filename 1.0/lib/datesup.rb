# -*- mode:ruby; coding:utf-8 -*-

# Copyright (c) 2002-2003 HORIKAWA Hisashi. All rights reserved.
# You can redistribute this software and/or modify it under the Ruby's License.

require "date"
if RUBY_VERSION < "1.9"
  require "parsedate"
end


if RUBY_VERSION < "1.9"
module ParseDate
  # 文字列を解析し, 時刻オブジェクトを得る
  # @param [String] datestr 日付表現の文字列
  # @return [Time] 時刻オブジェクト
  def self.get_time(datestr, cyear = nil)
    ary = parsedate(datestr, cyear)
    if ary[0] && (!ary[6] || ary[6] == 'GMT')
      return Time.gm(ary[0], ary[1], ary[2], ary[3], ary[4], ary[5])
    else
      return nil
    end
  end
end
end # RUBY_VERSION < "1.9"


## 文字列をTimeオブジェクトに変換する
# @param [String, nil] s 時刻を表す文字列
#   RFC 2616
#      Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
#      Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
#      Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
def get_time(s)
  return nil if !s
     
  d = s.split(/[, :-]+/)
  if CGI::RFC822_MONTHS.include?(d[2]) && d.size == 8
    # RFC 822
    return Time.gm(d[3], CGI::RFC822_MONTHS.index(d[2]) + 1, d[1], 
                   d[4], d[5], d[6])
  elsif Date::DAYNAMES.include?(d[0]) && d.size == 8
    # RFC 850
    return Time.gm(1900 + d[3], CGI::RFC822_MONTHS.index(d[2]) + 1, d[1],
                   d[4], d[5], d[6])
  elsif CGI::RFC822_MONTHS.include?(d[1]) && d.size == 7
    # asctime
    return Time.gm(d[6], CGI::RFC822_MONTHS.index(d[1]) + 1, d[2],
                   d[3], d[4], d[5])
  end
  return nil
end

class Date
  def Date.parse(str)
    raise TypeError, "The argument must be a String" if !str.is_a?(String)
    y, m, d = str.split("-")
    raise ArgumentError, "#{str} is not date." if !y || !m || !d
    return Date.new(y.to_i, m.to_i, d.to_i)
  end
end

# startを含む未来の1年間以内の日付を返す
# @return 日付が正しくない場合はnilを返す
def get_full_day(mon, day, start)
  raise TypeError, "month must be a Integer, but a #{mon.class}" if !mon.is_a?(Integer)
  raise TypeError, "day must be a Integer, but a #{day.class}" if !day.is_a?(Integer)
  raise TypeError if !start.is_a?(Date)

  if Date.valid_date?(start.year, mon, day) && 
                                (dt = Date.new(start.year, mon, day)) >= start
    return dt
  elsif Date.valid_date?(start.year + 1, mon, day) && 
                       (dt = Date.new(start.year + 1, mon, day)) < start + 365
    return dt
  else
    return nil
  end
end
