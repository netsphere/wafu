# -*- coding:utf-8 -*-

# Copyright (c) 2002 HORIKAWA Hisashi. All rights reserved.
#   mailto:vzw00011@nifty.ne.jp
#   http://www.nslabs.jp/

# テキストファイルでデータを保存する。
# キーは文字列ひとつのみ。


require "ftools"

#
# StrStore -- file based persistance mechanism.
# can store only strings.
#
# == Usage example:
#     ...same pstore.
#
class StrStore
  # The error type.
  class Error < RuntimeError
  end

  #
  # To construct a StrStore object.
  # private
  def initialize(file)
    raise TypeError, "filename is a #{file.class}. must be a String." if !file.is_a?(String)

    dir = File.dirname(file)
    if !FileTest.directory?(dir)
      raise ArgumentError, sprintf("directory %s does not exist", dir)
    end
    if !FileTest.writable?(dir)
      raise ArgumentError, sprintf("directory %s not writable", dir)
    end
    if FileTest.exist?(file) && !FileTest.readable?(file)
      raise ArgumentError, sprintf("file %s not readable", file)
    end

    @transaction = false
    @path = file
    @abort = false
    @rdonly = false
    @table = nil
  end
  attr_reader :path

  #
  # Retrieves a value.
  #
  def [](key)
    key_check(key)
    in_transaction
    raise IndexError, sprintf("key '%s' undefined", key) if !@table.key?(key)
    @table[key]
  end

  #
  # Store a string.
  #
  def []=(key, value)
    key_check(key)
    raise TypeError, "value is a #{value.class}, not string" if !value.is_a?(String)
    raise ArgumentError, sprintf("value %s includes CR or LF", value) if value =~ /[\r\n]/
    in_transaction_wr()
    @table[key] = value
  end

  #
  # Remove an object
  #
  def delete(key)
    key_check(key)
    in_transaction_wr()
    @table.delete key
  end

  def clear
    in_transaction_wr()
    @table.clear
  end

  #
  # Returns the names of all object
  #
  def keys
    in_transaction
    @table.keys
  end

  #
  # Returns true if the supplied _key_ is currently in the data store.
  #
  def key?(key)
    in_transaction
    @table.key? key
  end

  def to_a
    in_transaction
    @table.to_a
  end

  def commit
    in_transaction
    @abort = false
    throw :inistore_abort_transaction
  end

  def abort
    in_transaction
    @abort = true
    throw :inistore_abort_transaction
  end

  # Opens a new transaction for the data store.
  # 変更を完全に反映するか，トラブルが起こってもすべて破棄するために，
  # .tmp -> .new -> オリジナルという順で変更する。
  def transaction
    raise Error, "nested transaction" if @transaction
    begin
      @rdonly = false
      @abort = false
      @table = {}
      @transaction = true
      value = nil
      new_file = @path + ".new"

      File.open(@path, File::RDWR | File::CREAT) {|fp|
        fp.flock(File::LOCK_EX)
        commit_new() if FileTest.exist?(new_file)
        load_content(fp)
        begin
          catch(:inistore_abort_transaction) do
            value = yield(self)
          end
        rescue Exception
          @abort = true
          raise
        ensure
          if !@abort
            tmp_file = @path + ".tmp"
            File.open(tmp_file, "w") {|tmp_fp|
              dump_content(tmp_fp)
            }
            File.rename(tmp_file, new_file)
            commit_new()
          end
        end
      }
    ensure
      @transaction = false
      @table = nil
    end
    value
  end

  def transaction_ro
    raise Error, "nested transaction" if @transaction
    begin
      @rdonly = true
      @abort = false
      @table = {}
      @transaction = true
      value = nil
      new_file = @path + ".new"

      File.open(@path, File::RDWR | File::CREAT) {|fp|
        fp.flock(File::LOCK_SH)
        if FileTest.exist?(new_file)
          File.open(new_file) {|nf| load_content(nf)}
        else
          load_content(fp)
        end
        catch(:inistore_abort_transaction) do
          value = yield(self)
        end
      }
    ensure
      @transaction = false
      @table = nil
    end
    value
  end

  private
  def in_transaction()
    raise Error, "not in transaction" if !@transaction
  end

  def in_transaction_wr()
    in_transaction()
    raise Error, "in read-only transaction" if @rdonly
  end
  
  def key_check(key)
    raise TypeError, "key not string" if !key.is_a?(String)
    raise ArgumentError, sprintf("key '%s' includes '=' or space", key) if key =~ /[= \t\r\n]/
  end

  # Commits changes to the data store file.
  def commit_new()
    new_file = @path + ".new"
    if !File.copy(new_file, @path)
      raise IOError
    end
    File.unlink(new_file)
  end

  # Hashである@tableの内容を書き出す。サブクラスではオーバーライドすること
  def dump_content(io)
    @table.each {|k, v|
      io.print "#{k}=#{v}\n"
    }
  end

  # Hashである@tableの内容を更新する。サブクラスではオーバーライドすること
  def load_content(io)
    io.each_line {|line|
      k, v = line.gsub(/[\r\n]/, '').split('=', 2)
      @table[k] = v
    }
  end
end
